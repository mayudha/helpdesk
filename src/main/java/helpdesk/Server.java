package helpdesk;
/**
 * Developed by : 
 */
import static spark.Spark.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.mindrot.jbcrypt.BCrypt;
import org.postgresql.util.PGobject;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import com.fasterxml.jackson.databind.util.TypeKey;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import helpdesk.Utils.SocketPrint;
import helpdesk.WebSocket.WSAssets;
import helpdesk.WebSocket.WebSocketHandler;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

public class Server {
	
	private DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public Server(int port, int minThreads, int maxThreads, int timeOutMilis, 
			String dbHost, String dbPort, String dbName, String dbUser, String dbPass, 
			String dbSqlHost, String dbSqlPort, String dbSqlName, String dbSqlUser, String dbSqlPass,
			String dbHost1, String dbPort1, String dbName1, String dbUser1, String dbPass1) {
		
		// Apply config and start Server
		System.out.println("Server starting at Port: " + port + ", dbHost: " + dbHost + ", dbPort: " + dbPort);
		port(port);
		threadPool(maxThreads, minThreads, timeOutMilis);
		
//		String keyStoreLocation = "conf/abc.jks";
//		String keyStorePassword = "admin@rsudsda";
//		secure(keyStoreLocation, keyStorePassword, null, null);
		
		// Config Hikari pooling db connection
		HikariConfig config 	= new HikariConfig();
		config.setJdbcUrl("jdbc:postgresql://" + dbHost + ":" + dbPort + "/" + dbName + "?sslmode=disable");
		config.setDriverClassName(org.postgresql.Driver.class.getName());
		config.setUsername(dbUser);
		config.setPassword(dbPass);
		
		config.setMaximumPoolSize(10);
		config.setMinimumIdle(7);
		config.setAutoCommit(false);
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		
		DataSource dtSource 	= new HikariDataSource(config);

		HikariConfig config1 	= new HikariConfig();
		config1.setJdbcUrl("jdbc:postgresql://" + dbHost1 + ":" + dbPort1 + "/" + dbName1 + "?sslmode=disable");
		config1.setDriverClassName(org.postgresql.Driver.class.getName());
		config1.setUsername(dbUser1);
		config1.setPassword(dbPass1);
		
		config1.setMaximumPoolSize(10);
		config1.setMinimumIdle(7);
		config1.setAutoCommit(false);
		config1.addDataSourceProperty("cachePrepStmts", "true");
		config1.addDataSourceProperty("prepStmtCacheSize", "250");
		config1.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		
		DataSource dtSource1 	= new HikariDataSource(config1);
		
		// Config Hikari Mysql pooling db connection
		HikariConfig configMysql = new HikariConfig();
		String jdbcurl= "jdbc:mysql://" + dbSqlHost + ":" + dbSqlPort + "/" + dbSqlName + "?user=" + dbSqlUser + "&password=" + dbSqlPass + 
				"&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false&maxReconnects=1"
				+ "&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&zeroDateTimeBehavior=convertToNull";
		System.out.println(jdbcurl);
		configMysql.setJdbcUrl(jdbcurl);
//		config.setDriverClassName();
//		configMysql.setJdbcUrl("jdbc:mysql://" + dbSqlHost + ":" + dbSqlPort + "/" + dbSqlName);
//		configMysql.setUsername(dbSqlUser);
//		configMysql.setPassword(dbSqlPass);
		

		configMysql.setMaximumPoolSize(10);
		configMysql.setMinimumIdle(7);
		configMysql.setAutoCommit(false);
		configMysql.addDataSourceProperty("cachePrepStmts", "true");
		configMysql.addDataSourceProperty("prepStmtCacheSize", "250");
		configMysql.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		
		DataSource dtSourceMysql = new HikariDataSource(configMysql);
		
		
		// Sql2o
		Sql2o sql2o = new Sql2o(dtSource);
		Sql2o sql2o1 = new Sql2o(dtSource1);
//		Sql2o sql2oMysql = new Sql2o(jdbcurl, null, null);
		Sql2o sql2oMysql = new Sql2o(dtSourceMysql);
		
		
		// Start WebSocketService
		webSocket("/websocket/it", WebSocketHandler.class);
		webSocketIdleTimeoutMillis(864000000); //10 hari
		webSocket("/websocket/assets", WSAssets.class);
		init();
		
		apply();
		
		get("/SocketPrint", (req, res)->{
			res.type("application/json");
			
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getSocketPrint();
			return result;
		});
		
		get("/ClientIp", (req, res)->{
			res.type("application/json");
			
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");

			JsonObject jResult = new JsonObject();
			
			jResult.addProperty("status", 200);
			jResult.addProperty("ip_address", req.ip());
			return jResult.toString();
			
		});
		
		get("/MasterJenis", (req, res)->{
			res.type("application/json");
			
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterJenis(sql2o);
			return result;
		});
		
		get("/Master/Software", (req, res)->{
			res.type("application/json");
			
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterSoftware(sql2o);
			return result;
		});
		
		get("/Master/Software/Jenis", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterSoftwareJenis(sql2o);
			return result;
		});
		
		get("/MasterMerk", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterMerk(sql2o);
			return result;
		});
		
		get("/MasterDetail", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterDetail(sql2o);
			return result;
		});
		
		get("/MasterKategori", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterKategori(sql2o);
			return result;
		});
		
		get("/Master/NamaKabel", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterNamaKabel(sql2o);
			return result;
		});
		
		get("/Master/IT/Sparepart", (req, res)-> {
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterItSparepart(sql2o);
			return result;
		});
		
		get("/Master/IT/Sparepart_Second", (req, res)-> {
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterItSparepartSecond(sql2o);
			return result;
		});
				
		get("/Master/Requirement", (req, res)-> {
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterRequirement(sql2o);
			return result;
		});
		
		get("/Master/Ruang", (req, res)-> {
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterRuang(sql2o1);
			return result;
		});
		
		get("/Master/Sparepart", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterSparepart(sql2o);
			return result;
		});
		
		get("/Master/SOBarangRuangan/Detail/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterSOBarangRuanganDetail(sql2o, req);
			return result;
		});
		
		get("/Master/SOBarangRuangan/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterSOBarangRuangan(sql2o, req);
			return result;
		});
		
		get("/Master/Toner", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getMasterToner(sql2o);
			return result;
		});
		
		get("/ListInsert", (req, res)-> {
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getListInsert(sql2o, sql2oMysql);
			return result;
		});
		
		get("/ListPegawai/:user_id", (req, res)-> {
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getListPegawai(sql2o, sql2oMysql, req);
			return result;
		});
		
		get("/ListSparepart", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getListSparepart(sql2o, sql2oMysql);
			return result;
		});
		
		get("/Transaksi/Toner/History/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerHistory(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Pemakaian/Detail/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPemakaianDetail(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Pemakaian/List/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPemakaianList(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Pemakaian/ListAllUser/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPemakaianListAllUser(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Pengembalian/List/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPengembalianList(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PO/Tersedia/:id", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPOTersedia(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PoRetur/Tersedia/:id", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPoReturTersedia(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PO/Detail/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPODetail(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PO/List/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPOList(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PO/Penerimaan/Detail/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPOPenerimaanDetail(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Retur/Detail/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturDetail(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Retur/List/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturList(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Retur/Penerimaan/Inv/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturPenerimaanInv(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Retur/Penerimaan/ListInv/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturPenerimaanListInv(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Retur/Tersedia/Vendor/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturTersediaVendor(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Retur/Tersedia/Toner/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturTersediaToner(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PO/Penerimaan/List/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPOPenerimaanList(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/PO/Penerimaan/List/KomponenTambahan", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPOPenerimaanListKomponenTambahan(sql2o);
			return result;
		});
		
		get("/Transaksi/Toner/PO/Pemakaian/Tersedia/:id", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPemakaianTersedia(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Pengembalian/Tersedia/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPengembalianTersedia(sql2o, req);
			return result;
		});
		get("/Transaksi/Toner/PengeluaranBarang/User/*/*/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerPengeluaranBarangUser(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/ReturPihak3/Tersedia/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerReturPihak3Tersedia(sql2o, req);
			return result;
		});
		
		get("/Transaksi/Toner/Pengembalian/Verifikasi/Tersedia/*", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			String result = getTransaksiTonerVerifikasiReturTersedia(sql2o, req);
			return result;
		});
		
		get("/test", (req, res)-> {
			
//			System.out.println(sqlquery);
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			try(Connection conn= sql2o.open()){
				
				JsonObject jResult = new JsonObject();
				
				String sqlquery= "select * from master.ms_kategori";
				List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
				
				JsonArray jarr= new JsonArray();
								
				for (Map<String, Object> map : getReportData) {
					JsonObject jObj = new JsonObject();
					int intVal = (int) map.get("id");
					jObj.addProperty("id", intVal);
					String strVal = (String) map.get("nama_kategori_sparepart");
					jObj.addProperty("nama_kategori_sparepart", strVal);
					jarr.add(jObj);
				}
				
				jResult.addProperty("status", 200);
				jResult.add("data", jarr);
//				System.out.println("JsonArray= " + jarr.toString());
				return jResult.toString();
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}
		});
		
		get("/testmysql", (req, res)-> {
			
//			System.out.println(sqlquery);
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			res.header("Access-Control-Allow-Headers", "Content-Type");
			res.header("Access-Control-Max-Age", "86400");
			try(Connection conn= sql2oMysql.open()){
				String sqlquery							= "SELECT * FROM pegawai WHERE pegawai_id=2206";
				List<Map<String,Object>> getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
				JsonArray jarr							= new JsonArray();
				for (Map<String, Object> map : getReportData) {
					JsonObject jObj 					= new JsonObject();
					String strVal 						= (String) map.get("nama");
					jObj.addProperty("nama", strVal);
					jarr.add(jObj);

				}
				JsonObject jResult = new JsonObject();
				jResult.addProperty("status", 200);
				jResult.addProperty("message", "success");
				jResult.add("data", jarr);
//				System.out.println("JsonArray= " + jarr.toString());
				return jResult.toString();
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}
		});
		
		
		post("/InsertDetail", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Allow-Headers", "*");
			res.header("Access-Control-Max-Age", "86400");
			
			String strBody 		= req.body();
			JsonObject jObj 	= new Gson().fromJson(strBody, JsonObject.class);
			String jenisDetail 	= jObj.get("jenis_detail_nama").getAsString();
			String idJenis 		= jObj.get("idjenis").getAsString();
			String idJenisNama 	= jObj.get("idjenis_nama").getAsString();
			String result 		= postInsertDetail(sql2o, jenisDetail, idJenis, idJenisNama);
			return result;
		});
		
		post("/InsertJenis", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
//			res.header("Access-Control-Allow-Headers", "*");
			
			String strBody 		= req.body();
//			System.out.println(strBody);
			JsonObject jObj 	= new Gson().fromJson(strBody, JsonObject.class);
//			System.out.println(jObj.toString());
			String jenis 		= jObj.get("jenis").getAsString();
			String result 		= postInsertJenis(sql2o, jenis);
//			System.out.println(jenis);
			return result;
		});
		
		post("/InsertKategori", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
//			res.header("Access-Control-Allow-Headers", "*");
			
			String strBody 		= req.body();
			JsonObject jObj 	= new Gson().fromJson(strBody, JsonObject.class);
			String kategori 	= jObj.get("kategori").getAsString();
			String result 		= postInsertKategori(sql2o, kategori);
			return result;
		});
		
		post("/InsertMerk", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
//			res.header("Access-Control-Allow-Headers", "*");
			
			String strBody 		= req.body();
			JsonObject jObj 	= new Gson().fromJson(strBody, JsonObject.class);
			String merk 		= jObj.get("merk").getAsString();
			String result 		= postInsertMerk(sql2o, merk);
			return result;
		});
		
		post("/InsertSparepart", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
//			res.header("Access-Control-Allow-Headers", "*");
			
			String result 		= postInsertSparepart(sql2o, sql2oMysql, req);
			return result;
		});
		
		post("/Master/IT/Insert", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
//			res.header("Access-Control-Allow-Headers", "*");
			
			String result 		= postInsertMasterIT(sql2o, req);
			return result;
		});
		
		post("Insert/Master/Software", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
//			res.header("Access-Control-Allow-Headers", "*");
			
			String result 		= postInsertMasterSoftware(sql2o, req);
			return result;
		});
		
		post("Insert/Master/Software/Jenis", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postInsertMasterSoftwareJenis(sql2o, req);
			return result;
		});
		
		post("Insert/Master/StockOpname", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postInsertMasterStockOpname(sql2o, req);
			return result;
		});
		
		post("Insert/Master/Toner", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postInsertMasterToner(sql2o, req);
			return result;
		});
		
		post("Insert/Master/Tonernosp", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postInsertMasterTonerWOSP(sql2o, req);
			return result;
		});
		
		post("/Menu", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			String result = postMenu(sql2o, req);
			return result;
		});
		
		post("Transaksi/Log/CekToken", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiLogCekToken(sql2o, req);
			return result;
		});
		
		post("Transaksi/Log/Token", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiLogToken(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/Pemakaian", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPemakaian(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/Pengembalian", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPengembalian(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/Pengembalian/Verifikasi", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPengembalianVerifikasi(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/PengeluaranBarang", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPengeluaranBarang(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/PengeluaranBarang/UbahStatus", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPengeluaranBarangUbahStatus(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/PO", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPO(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/Podanretur", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPOnRetur(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/PO/Batal", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPOBatal(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/PO/Penerimaan", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPOPenerimaan(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/PO/PenerimaanNoToken", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerPOPenerimaanNoToken(sql2o, req);
			return result;
		});
		
		post("Transaksi/Toner/Retur", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			
			String result 		= postTransaksiTonerRetur(sql2o, req);
			return result;
		});
		
		post("cekabsen", (req, res)->{
			res.type("application/json");
//			res.header("Access-Control-Allow-Origin", "*");
			res.header("Server", "Oseng2 server 1.0.0");
			res.header("X-Powered-By", "ChahKangkung/1.0");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Methods", "POST");
			res.header("Access-Control-Max-Age", "86400");
			
			String strResult= req.body();
			System.out.println(strResult);
			
//			String result 		= postTransaksiLogToken(sql2o, req);
			return strResult;
		});
		
	}
	
	private String getMasterNamaKabel(Sql2o sql2o) {
		JsonObject jResult = new JsonObject();
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_nama_kabel ORDER BY id";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
										
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int intVal 			= (int) map.get("id");
				String namaGedung 	= (String) map.get("nama_gedung");
				String kode 		= (String) map.get("kode");
				
				
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_gedung", namaGedung);
				jObj.addProperty("kode", kode);

				jarr.add(jObj);
				
			}
			jResult.addProperty("status", 200);
			jResult.add("data", jarr);
			return jResult.toString();
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getListInsert(Sql2o sql2o, Sql2o sql2oMysql) {

		JsonObject jResult = new JsonObject();
		try(Connection conn= sql2o.open(); Connection connSql= sql2oMysql.open()){
			
			String sqlquery= "select * from master.ms_barang ORDER BY idbarang DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
										
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("idbarang");
				String strVal1 	= (String) map.get("kode_brg");
				String strVal2 	= (String) map.get("nama");
				String strVal3	= (String) map.get("satuan");
				String strVal4 	= (String) map.get("deskripsi");
				String strVal5 	= (String) map.get("jenis");
				String strVal6 	= (String) map.get("jenis_detail");
				String strVal7 	= (String) map.get("merk");
				String strVal8 	= (String) map.get("tipe");
				long intVal9 	= (long) map.get("user_id");
				
				jObj.addProperty("idbarang", intVal);
				jObj.addProperty("kode_brg", strVal1);
				jObj.addProperty("nama", strVal2);
				jObj.addProperty("satuan", strVal3);
				jObj.addProperty("deskripsi", strVal4);
				jObj.addProperty("jenis", strVal5);
				jObj.addProperty("jenis_detail", strVal6);
				jObj.addProperty("merk", strVal7);
				jObj.addProperty("tipe", strVal8);

				String mysqlQuery = "select * from pegawai where pegawai_id = :id";
				List<Map<String,Object>> getDataSql = connSql.createQuery(mysqlQuery).addParameter("id", intVal9).executeAndFetchTable().asList();
				
				Map<String, Object> mapSql 	= getDataSql.get(0);
				String strVal 				= (String) mapSql.get("nama");
				
				jObj.addProperty("UserID", strVal);

				jarr.add(jObj);
				
			}
			jResult.addProperty("status", 200);
			jResult.add("List_Insert", jarr);
			return jResult.toString();
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String getListPegawai(Sql2o sql2o, Sql2o sql2oMysql, Request req) {

		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
				
		String user_id			= req.params(":user_id");
		int id_user				= Integer.parseInt(user_id);
		
		JsonObject jResult = new JsonObject();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		try(Connection connSql= sql2oMysql.open()){
			
			String sqlquery= "SELECT p.PEGAWAI_ID, p.KTP, p.NAMA,p.ktp,REPLACE(p.nip, '.', '') AS nip,p.TGLAHIR,p.GELAR_DPN,p.GELAR_BLK,mu.idunit,"
					+ "mu.namaunit,p.FILE_PATH,AGAMA,ALAMAT,HP,TMLAHIR,LP,mjp.ID as jabatan_id,mjp.nama_jabatan,p.STATUS_KEPEGAWAIAN FROM pegawai p "
					+ "LEFT JOIN (SELECT * FROM (SELECT * FROM sdm.pgw_jabatan ORDER BY tmt DESC) AS pj GROUP BY pegawai_id) AS pj ON pj.pegawai_id=p.PEGAWAI_ID "
					+ "LEFT JOIN sdm.ms_jabatan_pegawai mjp ON mjp.ID=pj.JBT_ID "
					+ "LEFT JOIN dbaset.as_ms_unit mu ON pj.UNIT_ID = mu.idunit";
			List<Map<String,Object>> getReportData = connSql.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr 	= new JsonArray();
			int i			= 0;	
			
			for (Map<String, Object> map : getReportData) {
				
//				for (String name: map.keySet()){
//		            String key = name.toString();
////		            String value = map.get(name).toString();  
//		            System.out.println(key);// + " " + value);  
//				}
				
				JsonObject jObj = new JsonObject();
				
				int no 			= ++i;
				String nama 	= (String) map.get("nama");
				String namaUnit	= (String) map.get("namaunit");
				int idunit		= valueToIntOrZero(map, "idunit");
				long pegawaiId	= (long) map.get("pegawai_id");
				String filePath	= (String) map.get("file_path");
				
				jObj.addProperty("no", no);
				jObj.addProperty("nama", nama);
				jObj.addProperty("namaunit", namaUnit);
				jObj.addProperty("idunit", idunit);
				jObj.addProperty("pegawai_ID", pegawaiId);
				jObj.addProperty("value", nama);
				jObj.addProperty("label", nama);
				filePath= "https://rest.rsudsidoarjo.co.id/rest_dashboard/sdm/foto?berkas_path="+filePath;
//				System.out.println("link: " + filePath);
				jObj.addProperty("link", filePath);
				jarr.add(jObj);
			}
			jResult.addProperty("status", 200);
			jResult.add("data", jarr);
			return jResult.toString();
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String getListSparepart(Sql2o sql2o, Sql2o sql2oMysql) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open(); Connection connmysql= sql2oMysql.open();){
			
			JsonArray jarr = new JsonArray();
			
			String sqlquery= "select * from master.ms_sparepart ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 				= new JsonObject();
				long sparepart_id 				= (long) map.get("sparepart_id");
				String nama 					= (String) map.get("sparepart_nama");
				int port 					    = valueToIntOrZero(map, "sparepart_port");
				int sparepart_kategori 			= (int) map.get("sparepart_kategori");
				int sparepart_merk 				= (int) map.get("sparepart_merk");
				int sparepart_jenis 			= (int) map.get("sparepart_jenis");
				String sparepart_kapasitas 		= (String) map.get("sparepart_kapasitas");
				String sparepart_tipe 			= (String) map.get("sparepart_tipe");
				String sparepart_spesifikasi 	= (String) map.get("sparepart_spesifikasi");
				int userID 						= (int) map.get("user_id");
				
//				System.out.print(port);
				
				String sqlquerysub= "SELECT * from master.ms_kategori where id= :id";
				List<Map<String,Object>> getReportDataSub = conn.createQuery(sqlquerysub).
						addParameter("id", sparepart_kategori).executeAndFetchTable().asList();
				
				String nama_kategori_sparepart = (String) getReportDataSub.get(0).get("nama_kategori_sparepart");
				
				sqlquerysub	= "SELECT * from master.ms_merk where idmerk= :id";
				getReportDataSub = conn.createQuery(sqlquerysub).
						addParameter("id", sparepart_merk).executeAndFetchTable().asList();
				
				String merk = (String) getReportDataSub.get(0).get("merk");
				
				sqlquerysub	= "SELECT jenis from master.ms_jenis where idjenis= :id";
				getReportDataSub = conn.createQuery(sqlquerysub).
						addParameter("id", sparepart_jenis).executeAndFetchTable().asList();
				
				String jenis = (String) getReportDataSub.get(0).get("jenis");
				
				String nama_port 		= "";
				if(port > 0) {
					sqlquerysub			= "SELECT nama_port from master.ms_port where id= :id";
					getReportDataSub 	= conn.createQuery(sqlquerysub).
							addParameter("id", port).executeAndFetchTable().asList();
					
					nama_port 			= (String) getReportDataSub.get(0).get("nama_port");
				}
				
				String mysqlQuery 		= "select * from pegawai where pegawai_id = :id";
				List<Map<String,Object>> getDataSql = connmysql.createQuery(mysqlQuery).addParameter("id", userID)
						.executeAndFetchTable().asList();
				
				Map<String, Object> mapSql = getDataSql.get(0);
				String userid = (String) mapSql.get("nama");
								
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("nama", nama);
				jObj.addProperty("port", port);
				jObj.addProperty("nama_kategori_sparepart", nama_kategori_sparepart);
				jObj.addProperty("sparepart_kategori", sparepart_kategori);
				jObj.addProperty("sparepart_merk", sparepart_merk);
				jObj.addProperty("merk", merk);
				jObj.addProperty("sparepart_jenis", sparepart_jenis);
				jObj.addProperty("jenis_nama", jenis);
				jObj.addProperty("nama_port", nama_port);
				jObj.addProperty("sparepart_kapasistas", sparepart_kapasitas);
				jObj.addProperty("sparepart_tipe", sparepart_tipe);
				jObj.addProperty("sparepart_spesifikasi", sparepart_spesifikasi);
				jObj.addProperty("UserID", userid);
				jObj.addProperty("value", nama);
				jObj.addProperty("label", nama);
				
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("List_Insert", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	
	private String getMasterDetail(Sql2o sql2o) {

		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select * from master.ms_jenis_detail ORDER BY jenis_detail_id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr	= new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("jenis_detail_id");
				String strVal 	= (String) map.get("jenis_detail_nama");
				String strVal1 	= (String) map.get("idjenis");
				String strVal2 	= (String) map.get("idjenis_nama");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama", strVal);
				jObj.addProperty("idjenis", strVal1);
				jObj.addProperty("idjenis_nama", strVal2);
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_detail", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterItSparepart(Sql2o sql2o) {
		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_sparepart where aktif=1 ";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarrdatamemory		= new JsonArray();
			JsonArray jarrdataprocessor		= new JsonArray();
			JsonArray jarrdataharddisk		= new JsonArray();
			JsonArray jarrdatapowersupply	= new JsonArray();
			JsonArray jarrdatacddvd			= new JsonArray();
			JsonArray jarrdatamotherboard	= new JsonArray();
			JsonArray jarrdatakeyboard		= new JsonArray();
			JsonArray jarrdatamouse			= new JsonArray();
			JsonArray jarrdatavga			= new JsonArray();
			JsonArray jarrdatalancard		= new JsonArray();
			JsonArray jarrdataswitch		= new JsonArray();
			JsonArray jarrdatabarang		= new JsonArray();
			JsonArray jarrdatajenis			= new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int kategori 	= (int) map.get("sparepart_kategori");
				
				long sparepart_id	 	= (long) map.get("sparepart_id");
				String sparepart_nama 	= (String) map.get("sparepart_nama");
				int sparepart_kategori 	= (int) map.get("sparepart_kategori");
				int sparepart_merk 		= (int) map.get("sparepart_merk");
				
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("sparepart_nama", sparepart_nama);
				jObj.addProperty("sparepart_kategori", sparepart_kategori);
				jObj.addProperty("sparepart_merk", sparepart_merk);
				
				switch (kategori) {
					case 1:
						jarrdatamemory.add(jObj);
						break;
					case 2:
						jarrdataprocessor.add(jObj);
						break;
					case 3:
						jarrdataharddisk.add(jObj);
						break;
					case 4:
						jarrdatapowersupply.add(jObj);
						break;
					case 5:
						jarrdatacddvd.add(jObj);
						break;
					case 6:
						jarrdatamotherboard.add(jObj);
						break;
					case 7:
						jarrdatakeyboard.add(jObj);
						break;
					case 8:
						jarrdatamouse.add(jObj);
						break;
					case 9:
						jarrdatavga.add(jObj);
						break;
					case 10:
						jarrdatalancard.add(jObj);
						break;
		
					default:
						break;
				}
				
			}
			
			sqlquery= "select * from master.ms_switch where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int id 				= (int) map.get("id");
				String nama_switch 	= (String) map.get("nama_switch");
				String ip_switch 	= (String) map.get("ip_switch");
				
				jObj.addProperty("id", id);
				jObj.addProperty("nama_switch", nama_switch);
				jObj.addProperty("ip_switch", ip_switch);
				jarrdataswitch.add(jObj);
			}
			
			sqlquery= "select * from master.ms_jenis where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int idjenis 	= (int) map.get("idjenis");
				String jenis 	= (String) map.get("jenis");
				
				jObj.addProperty("idjenis", idjenis);
				jObj.addProperty("jenis", jenis);
				jarrdatajenis.add(jObj);
			}
			
			sqlquery= "select * from master.ms_barang where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 		= new JsonObject();
				int idbarang 			= (int) map.get("idbarang");
				String kode_brg 		= (String) map.get("kode_brg");
				String nama 			= (String) map.get("nama");
				String satuan 			= (String) map.get("satuan");
				String deskripsi 		= (String) map.get("deskripsi");
				String jenis_id 		= (String) map.get("jenis_id");
				String jenis 			= (String) map.get("jenis");
				String jenis_detail 	= (String) map.get("jenis_detail");
				String jenis_detail_id 	= (String) map.get("jenis_detail_id");
				int aktif 				= (int) map.get("aktif");
				String merk				= (String) map.get("merk");
				String tipe 			= (String) map.get("tipe");
				
				jObj.addProperty("idbarang", idbarang);
				jObj.addProperty("kode_brg", kode_brg);
				jObj.addProperty("nama", nama);
				jObj.addProperty("satuan", satuan);
				jObj.addProperty("deskripsi", deskripsi);
				jObj.addProperty("jenis_id", Integer.parseInt(jenis_id));
				jObj.addProperty("jenis", jenis);
				jObj.addProperty("jenis_detail", jenis_detail);
				jObj.addProperty("jenis_detail_id", Integer.parseInt(jenis_detail_id));
				jObj.addProperty("aktif", aktif);
				jObj.addProperty("merk", merk);
				jObj.addProperty("tipe", tipe);
				jarrdatabarang.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("memory", jarrdatamemory);
			jResult.add("processor", jarrdataprocessor);
			jResult.add("harddisk", jarrdataharddisk);
			jResult.add("psu", jarrdatapowersupply);
			jResult.add("cddvd", jarrdatacddvd);
			jResult.add("motherboard", jarrdatamotherboard);
			jResult.add("keyboard", jarrdatakeyboard);
			jResult.add("mouse", jarrdatamouse);
			jResult.add("vga", jarrdatavga);
			jResult.add("lancard", jarrdatalancard);
			jResult.add("switch", jarrdataswitch);
			jResult.add("data_jenis", jarrdatajenis);
			jResult.add("data_barang", jarrdatabarang);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getMasterItSparepartSecond(Sql2o sql2o) {
		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_sparepart where aktif=1 ";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarrdataall		= new JsonArray();
			JsonArray jarrdataperuntukan= new JsonArray();
			JsonArray jarrdataswitch	= new JsonArray();
			JsonArray jarrdatabarang	= new JsonArray();
			JsonArray jarrdatajenis		= new JsonArray();
			JsonArray jarrdatasoftware	= new JsonArray();
			JsonArray jarrdatavlan		= new JsonArray();
			JsonArray jarrswitchport	= new JsonArray();
			JsonArray jarrnamakabel		= new JsonArray();
			
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				//int kategori 	= (int) map.get("sparepart_kategori");
				
				long sparepart_id	 			= (long) map.get("sparepart_id");
				String sparepart_nama 			= (String) map.get("sparepart_nama");
				int sparepart_kategori 			= (int) map.get("sparepart_kategori");
				int sparepart_merk 				= (int) map.get("sparepart_merk");
				String sparepart_spesifikasi 	= (String) map.get("sparepart_spesifikasi");
				int sparepart_jenis 			= (int) map.get("sparepart_jenis");
				int sparepart_port				= valueToIntOrZero(map, "sparepart_port");
				
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("sparepart_nama", sparepart_nama);
				jObj.addProperty("sparepart_kategori", sparepart_kategori);
				jObj.addProperty("sparepart_merk", sparepart_merk);
				jObj.addProperty("sparepart_spesifikasi", sparepart_spesifikasi);
				jObj.addProperty("sparepart_jenis", sparepart_jenis);
				jObj.addProperty("sparepart_port", sparepart_port);
				jObj.addProperty("value", sparepart_nama);
				jObj.addProperty("label", sparepart_nama);
				
				jarrdataall.add(jObj);
						
			}
			
			sqlquery= "select * from master.ms_switch_detail where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int id 				= (int) map.get("id");
				int idswitch 			= (int) map.get("id_switch");
				String nama 		= (String) map.get("nama_switch");
				int portused 			= (int) map.get("port_used");
				
				jObj.addProperty("id", id);
				jObj.addProperty("id_switch", idswitch);
				jObj.addProperty("nama_switch", nama);
				jObj.addProperty("port_used", portused);
				jObj.addProperty("value", nama);
				jObj.addProperty("lebel", nama);
				jarrswitchport.add(jObj);
			}
			
			sqlquery= "select * from master.ms_mapping_barang_ruangan where kode_barang ilike '%6/%' ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int id 				= (int) map.get("id");
				String nama 		= (String) map.get("nama_barang");
				String ip_switch 	= (String) map.get("ip_lan");
				int jumlah_port		= (int) map.get("jumlah_port");
				String nama_lokasi		= (String) map.get("nama_ruangan");
				
				jObj.addProperty("id", id);
				jObj.addProperty("label", nama_lokasi + " - " + ip_switch + " - " + nama);
				jObj.addProperty("value", nama_lokasi + " - " + ip_switch + " - " + nama);
				jObj.addProperty("ip_switch", ip_switch);
				jObj.addProperty("jumlah_port", jumlah_port);
				
				
				JsonArray jarrAvailablePort = new JsonArray();
				for(int j=1; j< (jumlah_port+1); j++) {
					jarrAvailablePort.add(j);
				}
				
//				List<Integer> lsUsedPort = new ArrayList<Integer>();
				
				for (JsonElement jsonElement : jarrswitchport) {
					JsonObject jObjElement = (JsonObject) jsonElement;
					
					if(jObjElement.get("id_switch").getAsInt() == id) {
						int intPortUsed = jObjElement.get("port_used").getAsInt();
						 
						for(int i=0; i<jarrAvailablePort.size(); i++) {
							if(jarrAvailablePort.get(i).getAsInt() == intPortUsed) {
								jarrAvailablePort.remove(i);
							}
						}
					}
				}
				
				jObj.add("available_port", jarrAvailablePort);
				jarrdataswitch.add(jObj);
				
			}
			
			sqlquery= "select * from master.ms_vlan where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int id 				= (int) map.get("id");
				int idvlan 			= (int) map.get("id_vlan");
				String label 		= (String) map.get("nama");
				
				jObj.addProperty("id", id);
				jObj.addProperty("id_vlan", idvlan);
				jObj.addProperty("label", label);
				jObj.addProperty("value", label);
				jarrdatavlan.add(jObj);
			}
			
			sqlquery= "select * from master.ms_jenis where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int idjenis 	= (int) map.get("idjenis");
				String jenis 	= (String) map.get("jenis");
				
				jObj.addProperty("idjenis", idjenis);
				jObj.addProperty("jenis", jenis);
				jObj.addProperty("value", jenis);
				jObj.addProperty("label", jenis);
				
				jarrdatajenis.add(jObj);
			}
			
			sqlquery= "select * from master.ms_peruntukan where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int id 				= (int) map.get("id");
				String peruntukan 	= (String) map.get("peruntukan");
				
				jObj.addProperty("id", id);
				jObj.addProperty("peruntukan", peruntukan);
				jObj.addProperty("label", peruntukan);
				jObj.addProperty("value", peruntukan);
				jarrdataperuntukan.add(jObj);
			}
			
			sqlquery= "select * from master.ms_barang where aktif=1 ";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 		= new JsonObject();
				int idbarang 			= (int) map.get("idbarang");
				String kode_brg 		= (String) map.get("kode_brg");
				String nama 			= (String) map.get("nama");
				String satuan 			= (String) map.get("satuan");
				String deskripsi 		= (String) map.get("deskripsi");
				String jenis_id 		= (String) map.get("jenis_id");
				String jenis 			= (String) map.get("jenis");
				String jenis_detail 	= (String) map.get("jenis_detail");
				String jenis_detail_id 	= (String) map.get("jenis_detail_id");
				int aktif 				= (int) map.get("aktif");
				String merk				= (String) map.get("merk");
				String tipe 			= (String) map.get("tipe");
				
				jObj.addProperty("idbarang", idbarang);
				jObj.addProperty("kode_brg", kode_brg);
				jObj.addProperty("nama", nama);
				jObj.addProperty("satuan", satuan);
				jObj.addProperty("deskripsi", deskripsi);
				jObj.addProperty("jenis_id", Integer.parseInt(jenis_id));
				jObj.addProperty("jenis", jenis);
				jObj.addProperty("jenis_detail", jenis_detail);
				jObj.addProperty("jenis_detail_id", Integer.parseInt(jenis_detail_id));
				jObj.addProperty("aktif", aktif);
				jObj.addProperty("merk", merk);
				jObj.addProperty("tipe", tipe);
				jarrdatabarang.add(jObj);
			}
			
			sqlquery		= "select * from master.ms_software";
			getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
	
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("software_id");
				String strVal 	= (String) map.get("software_nama");
				int intVal1 	= (int) map.get("jenis_id");
				String strVal1	= (String) map.get("jenis_nama");
				String strVal2	= (String) map.get("software_spesifikasi");
				String strVal3	= (String) map.get("versi");
				String strVal4	= valueToStringOrEmptyString(map, "linkurl");
				
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_software", strVal);
				jObj.addProperty("jenis_id", intVal1);
				jObj.addProperty("jenis_nama", strVal1);
				jObj.addProperty("spesifikasi", strVal2);
				jObj.addProperty("label", strVal + " " + strVal3);
				jObj.addProperty("value", strVal + " " + strVal3);
				jObj.addProperty("versi", strVal3);
				jObj.addProperty("linkurl", strVal4);
				
				jarrdatasoftware.add(jObj);
			}
			
			sqlquery= "select * from master.ms_nama_kabel ORDER BY id";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
										
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int intVal 			= (int) map.get("id");
				String namaGedung 	= (String) map.get("nama_gedung");
				String kode 		= (String) map.get("kode");
				
				
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_gedung", namaGedung);
				jObj.addProperty("kode", kode);
				jObj.addProperty("value", kode+"-"+namaGedung);
				jObj.addProperty("label", kode+"-"+namaGedung);

				jarrnamakabel.add(jObj);
				
			}
						
			jResult.addProperty("status", 200);
			jResult.add("all_sparepart", jarrdataall);
			jResult.add("data_peruntukan", jarrdataperuntukan);
			jResult.add("switch", jarrdataswitch);
			jResult.add("vlan", jarrdatavlan);
			jResult.add("data_jenis", jarrdatajenis);
			jResult.add("data_barang", jarrdatabarang);
			jResult.add("data_software", jarrdatasoftware);
			jResult.add("nama_kabel", jarrnamakabel);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getMasterJenis(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_jenis";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("idjenis");
				String strVal 	= (String) map.get("jenis");
				jObj.addProperty("idjenis", intVal);
				jObj.addProperty("jenis", strVal);
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_jenis", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getMasterSoftwareJenis(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_jenis_software order by jenis_id desc";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("jenis_id");
				String strVal 	= (String) map.get("jenis_nama");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_software", strVal);
				
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("software_jenis", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
			
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterKategori(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_kategori ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("id");
				String strVal 	= (String) map.get("nama_kategori_sparepart");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_kategori_sparepart", strVal);
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_kategori", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterMerk(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_merk";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("idmerk");
				String strVal 	= (String) map.get("merk");
				jObj.addProperty("idmerk", intVal);
				jObj.addProperty("merk", strVal);
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_merk", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterRequirement(Sql2o sql2o) {
		
		// Tampungan Json untuk respon
		JsonObject jResult = new JsonObject();
		try(Connection conn= sql2o.open()){
			
			// query satuan
			String sqlquery= "select * from master.ms_satuan";
			List<Map<String,Object>> getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
			JsonArray jArrSatuan= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj 					= new JsonObject();
				int intVal 							= (int) map.get("id");
				String strVal 						= (String) map.get("nama");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama", strVal);
				jArrSatuan.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("satuan", jArrSatuan);
			
			// query jenis
			sqlquery= "select * from master.ms_jenis where aktif=1";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			jArrSatuan= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj 	= new JsonObject();
				int intVal 			= (int) map.get("idjenis");
				String strVal 		= (String) map.get("jenis");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama", strVal);
				
				jArrSatuan.add(jObj);
			}
			jResult.add("jenis", jArrSatuan);
			
			// query detail jenis
			sqlquery			= "select * from master.ms_jenis_detail where aktif=1";
			getReportData 		= conn.createQuery(sqlquery).executeAndFetchTable().asList();
			jArrSatuan			= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj = new JsonObject();
				
				int intVal 		= (int) map.get("jenis_detail_id");
				String strVal 	= (String) map.get("jenis_detail_nama");
				String intVal2	= (String) map.get("idjenis");
				String strVal2 	= (String) map.get("idjenis_nama");
				jObj.addProperty("jenis_detail_id", intVal);
				jObj.addProperty("jenis_detail_nama", strVal);
				jObj.addProperty("idjenis", Integer.parseInt(intVal2));
				jObj.addProperty("nama", strVal2);
				
				jArrSatuan.add(jObj);
			}
			jResult.add("detail_jenis", jArrSatuan);
			
			// query merk
			sqlquery		= "select * from master.ms_merk where aktif=1";
			getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
			jArrSatuan		= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj = new JsonObject();
				
				int intVal 		= (int) map.get("idmerk");
				String strVal 	= (String) map.get("merk");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama", strVal);
				
				jArrSatuan.add(jObj);
			}
			jResult.add("merk", jArrSatuan);
			
			// query software jenis
			sqlquery		= "select * from master.ms_jenis_software where aktif=1 order by jenis_id desc";
			getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
			jArrSatuan		= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj = new JsonObject();
				
				int intVal 		= (int) map.get("jenis_id");
				String strVal 	= (String) map.get("jenis_nama");
				jObj.addProperty("jenis_id", intVal);
				jObj.addProperty("jenis_nama", strVal);
				
				jArrSatuan.add(jObj);
			}
			jResult.add("software_jenis", jArrSatuan);
			
			// query software jenis
			sqlquery		= "select * from master.ms_software where aktif=1 order by software_id desc";
			getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
			jArrSatuan		= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj = new JsonObject();
				
				int intVal 		= (int) map.get("software_id");
				String strVal 	= (String) map.get("software_nama");
				int intVal1 	= (int) map.get("jenis_id");
				String strVal1 	= (String) map.get("jenis_nama");
				String strVal2 	= (String) map.get("software_spesifikasi");
				String strVal3 	= (String) map.get("versi");
				jObj.addProperty("software_id", intVal);
				jObj.addProperty("software_nama", strVal);
				jObj.addProperty("jenis_id", intVal1);
				jObj.addProperty("jenis_nama", strVal1);
				jObj.addProperty("software_spesifikasi", strVal2);
				jObj.addProperty("versi", strVal3);
				
				jArrSatuan.add(jObj);
			}
			jResult.add("software", jArrSatuan);
			
			// query sparepart toner
			sqlquery		= "select * from master.ms_sparepart where sparepart_kategori=11 order by sparepart_nama";
			getReportData 	= conn.createQuery(sqlquery).executeAndFetchTable().asList();
			jArrSatuan		= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj = new JsonObject();
				
				long sparepart_id 			= (long) map.get("sparepart_id");
				int sparepart_kategori 		= (int) map.get("sparepart_kategori");
				int sparepart_merk 			= (int) map.get("sparepart_merk");
				int sparepart_jenis			= (int) map.get("sparepart_jenis");
				String sparepart_tipe 		= (String) map.get("sparepart_tipe");
				String sparepart_nama 		= (String) map.get("sparepart_nama");
//				String warna 				= (String) map.get("warna");
//				String keterangan 			= (String) map.get("keterangan");
//				int originalitas 			= (int) map.get("originalitas");
				
				
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("sparepart_kategori", sparepart_kategori);
				jObj.addProperty("sparepart_merk", sparepart_merk);
				jObj.addProperty("sparepart_jenis", sparepart_jenis);
				jObj.addProperty("sparepart_tipe", sparepart_tipe);
				jObj.addProperty("sparepart_nama", sparepart_nama);
//				jObj.addProperty("warna", warna);
//				jObj.addProperty("keterangan", keterangan);
//				jObj.addProperty("originalitas", originalitas);
				
				jObj.addProperty("value", sparepart_nama);
				jObj.addProperty("label", sparepart_nama);
				
				jArrSatuan.add(jObj);
			}
			jResult.add("sparepart_toner", jArrSatuan);
			
			
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getMasterRuang(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from aset_master.ms_ruang";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int id 				= (int) map.get("id");
				int gedung	 		= (int) map.get("gedung");
				Integer lantai 		= valueToIntOrNull(map, "lantai");
//				Object oRuang	= map.get("ruang");
//				int ruang 		= oRuang == null ? null : (int) oRuang;
				Integer ruang		= valueToIntOrNull(map, "ruang");
				String title 		= (String) map.get("nama");
				Integer parent_id 	= valueToIntOrNull(map, "parent_id");
				Integer user_id 	= valueToIntOrNull(map, "user_id");
				
				jObj.addProperty("id", id);
				jObj.addProperty("gedung", gedung);
				jObj.addProperty("lantai", lantai);
				jObj.addProperty("ruang", ruang);
				jObj.addProperty("title", title);
				jObj.addProperty("parent_id", parent_id);
				jObj.addProperty("user_id", user_id);
				jObj.addProperty("label", title);
				jObj.addProperty("value", title);
				
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_lokasi", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterSoftware(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_software order by software_nama";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("software_id");
				String strVal 	= (String) map.get("software_nama");
				int intVal1 	= (int) map.get("jenis_id");
				String strVal1	= (String) map.get("jenis_nama");
				String strVal2	= (String) map.get("software_spesifikasi");
				String strVal3	= (String) map.get("versi");
				String strVal4	= valueToStringOrEmptyString(map, "linkurl");
				
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_software", strVal);
				jObj.addProperty("jenis_id", intVal1);
				jObj.addProperty("jenis_nama", strVal1);
				jObj.addProperty("spesifikasi", strVal2);
				jObj.addProperty("versi", strVal3);
				jObj.addProperty("linkurl", strVal4);
				
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("software", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
			
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterSparepart(Sql2o sql2o) {

		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jarrKategori	= new JsonArray();
			JsonArray jarrJenis		= new JsonArray();
			JsonArray jarrPort		= new JsonArray();
			JsonArray jarrMerk		= new JsonArray();
			
			String sqlquery		= "select * from master.ms_kategori";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("id");
				String strVal 	= (String) map.get("nama_kategori_sparepart");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_kategori_sparepart", strVal);
				jarrKategori.add(jObj);
			}
			
			sqlquery= "select * from master.ms_merk";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int intVal 			= (int) map.get("idmerk");
				String strVal 		= (String) map.get("merk");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama", strVal);
				jarrMerk.add(jObj);
			}
			
			sqlquery= "select * from master.ms_jenis";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj 	= new JsonObject();
				int intVal 			= (int) map.get("idjenis");
				String strVal 		= (String) map.get("jenis");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama", strVal);
				jarrJenis.add(jObj);
			}
			
			sqlquery= "select * from master.ms_port";
			getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int intVal 		= (int) map.get("id");
				String strVal 	= (String) map.get("nama_port");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_port", strVal);
				jarrPort.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("list_jenis", jarrJenis);
			jResult.add("list_port", jarrPort);
			jResult.add("list_merk", jarrMerk);
			jResult.add("list_kategori", jarrKategori);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getMasterToner(Sql2o sql2o) {
		JsonObject jResult = new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "select * from master.ms_toner where aktif=1 order by serial_number DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				int id 					= (int) map.get("id");
				String toner_nama 		= (String) map.get("toner_nama");
				int sparepart_id 		= (int) map.get("sparepart_id");
				String sparepart_nama 	= (String) map.get("sparepart_nama");
				String serial_number 	= (String) map.get("serial_number");
				String warna 			= (String) map.get("warna");
				String keterangan 		= (String) map.get("keterangan");
				int originalitas 		= valueToIntOrZero(map, "originalitas");
//				String str_toner_id		= (String) map.get("toner_id");
				jObj.addProperty("id", id);
				jObj.addProperty("toner_nama", toner_nama);
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("sparepart_nama", sparepart_nama);
				jObj.addProperty("serial_number", serial_number);
				jObj.addProperty("warna", warna);
				jObj.addProperty("keterangan", keterangan);
				jObj.addProperty("originalitas", originalitas);
				
//				jObj.addProperty("toner_id", str_toner_id);
				jObj.addProperty("value", toner_nama);
				jObj.addProperty("label", toner_nama);
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("toner", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getMasterSOBarangRuangan(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int status_po			= Integer.parseInt(params[1]);
		
//		Date tglAwal;
//		Date tglAkhir;
//		
//		try {
//			tglAwal					= dateFormat.parse(params[1]);
//			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//			
//			tglAkhir				= new Date();
//			// Create a calendar object with today date. Calendar is in java.util pakage.
//		    Calendar calendar = Calendar.getInstance();
//
//		    // Move calendar to yesterday
//		    calendar.add(Calendar.DATE, -1);
//			tglAwal					= calendar.getTime();
//			
//		}
		
		JsonObject jResult		= new JsonObject();
//		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
//		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		
		try(Connection conn= sql2o.open()){
//			String sqlquery= "select * from master.ms_mapping_barang_ruangan where aktif=1 AND tgl_act >= :tglAwal AND tgl_act < :tglAkhir order by id DESC";
			String sqlquery= "select * from master.ms_mapping_barang_ruangan where aktif=1 order by id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
//					.addParameter("tglAwal", tglAwal)
//					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj 		= new JsonObject();
				
				String noMapping 		= (String) map.get("no_mapping");
				int idbarang 			= valueToIntOrZero(map, "idbarang");
				String namaBarang 		= valueToStringOrEmptyString(map, "nama_barang");
				String url 				= valueToStringOrEmptyString(map, "url");
				int peruntukan 			= valueToIntOrZero(map, "peruntukan");
				int idRuangan 			= valueToIntOrZero(map, "id_ruangan");
				String namaRuangan 		= valueToStringOrEmptyString(map, "nama_ruangan");
				int jenisPerangkat 		= valueToIntOrZero(map, "jenis_perangkat");
				String kib 				= valueToStringOrEmptyString(map, "kib");
				Date tglAct 			= (Date) map.get("tgl_act");
				int aktif 				= (int) map.get("aktif");
				String pegawaiNama 		= valueToStringOrEmptyString(map, "pegawai_nama");
				String pegawaiJabatan 	= valueToStringOrEmptyString(map, "pegawai_jabatan");
				
				String ipLan 			= valueToStringOrEmptyString(map, "ip_lan");
				String macLan			= valueToStringOrEmptyString(map, "mac_lan");
				String ipWifi 			= valueToStringOrEmptyString(map, "ip_wifi") ;
				String macWifi			= valueToStringOrEmptyString(map, "mac_wifi");
				
				int idSwitch 			= valueToIntOrZero(map, "id_switch");
				int idVlan 				= valueToIntOrZero(map, "id_vlan");
				int statusOnline 		= valueToIntOrZero(map, "status_online");
				int id 					= valueToIntOrZero(map, "status_online");
				int userId 				= valueToIntOrZero(map, "user_id");
				String kodeBarang 		= valueToStringOrEmptyString(map, "kode_barang");
				String serialNumber 	= valueToStringOrEmptyString(map, "serial_number");
				String ketRuangan 		= valueToStringOrEmptyString(map, "ket_ruangan");
				String userNama 		= valueToStringOrEmptyString(map, "user_nama");
				int rekananId 			= valueToIntOrZero(map, "rekanan_id");
				String rekananNama 		= valueToStringOrEmptyString(map, "rekanan_nama");
				String noAsset 			= valueToStringOrEmptyString(map, "no_asset");
				int jumlahPort			= valueToIntOrZero(map, "jumlah_port");
				String spesifikasi		= valueToStringOrEmptyString(map, "spesifikasi");
				int sfp					= valueToIntOrZero(map, "sfp");
				int sfpPlus				= valueToIntOrZero(map, "sfp_plus");
				String areaCoverage		= valueToStringOrEmptyString(map, "area_coverage");
				int jenisId				= valueToIntOrZero(map, "jenis_id");
				
				
				
				jObj.addProperty("no_mapping", noMapping);
				jObj.addProperty("id_barang", idbarang);
				jObj.addProperty("nama_barang", namaBarang);
				jObj.addProperty("url", url);
				jObj.addProperty("peruntukan", peruntukan);
				jObj.addProperty("id_ruangan", idRuangan);
				
				jObj.addProperty("namaRuangan", namaRuangan);
				jObj.addProperty("jenis_perangkat", jenisPerangkat);
				jObj.addProperty("kib", kib);
				jObj.addProperty("tgl_act", dateFormat.format(tglAct));
				jObj.addProperty("aktif", aktif);				
				jObj.addProperty("pegawaiNama", pegawaiNama);
				jObj.addProperty("pegawaiJabatan", pegawaiJabatan);
				
				jObj.addProperty("ip_lan", ipLan);
				jObj.addProperty("mac_lan", macLan);
				jObj.addProperty("ip_wifi", ipWifi);
				jObj.addProperty("mac_wifi", macWifi);
				
				jObj.addProperty("id_switch", idSwitch);
				jObj.addProperty("id_vlan", idVlan);
				jObj.addProperty("status_online", statusOnline);
				jObj.addProperty("id", id);
				jObj.addProperty("user_id", userId);
				
				jObj.addProperty("kode_barang", kodeBarang);
				jObj.addProperty("serial_number", serialNumber);
				jObj.addProperty("ket_ruangan", ketRuangan);
				jObj.addProperty("user_nama", userNama);
				jObj.addProperty("rekanan_id", rekananId);
				jObj.addProperty("rekanan_nama", rekananNama);
				jObj.addProperty("no_asset", noAsset);
				
				jObj.addProperty("jumlah_port", jumlahPort);
				jObj.addProperty("spesifikasi", spesifikasi);
				jObj.addProperty("sfp", sfp);
				jObj.addProperty("sfp_plus", sfpPlus);
				jObj.addProperty("area_coverage", areaCoverage);
				jObj.addProperty("jenis_id", jenisId);
				
				jObj.addProperty("value", noMapping);
				jObj.addProperty("label", noMapping);
				
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String getMasterSOBarangRuanganDetail(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String noMapping		= params[1];
		
//		Date tglAwal;
//		Date tglAkhir;
//		
//		try {
//			tglAwal					= dateFormat.parse(params[1]);
//			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//			
//			tglAkhir				= new Date();
//			// Create a calendar object with today date. Calendar is in java.util pakage.
//		    Calendar calendar = Calendar.getInstance();
//
//		    // Move calendar to yesterday
//		    calendar.add(Calendar.DATE, -1);
//			tglAwal					= calendar.getTime();
//			
//		}
		
		JsonObject jResult		= new JsonObject();
//		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
//		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		
		try(Connection conn= sql2o.open()){
//			String sqlquery= "select * from master.ms_mapping_barang_ruangan where aktif=1 AND tgl_act >= :tglAwal AND tgl_act < :tglAkhir order by id DESC";
			String sqlquery= "select * from master.ms_mapping_barang_ruangan_detil where status=1 AND no_mapping= :noMapping order by id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
//					.addParameter("tglAwal", tglAwal)
					.addParameter("noMapping", noMapping)
					.executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				
				JsonObject jObj 		= new JsonObject();
				
				long id 				= (long) map.get("id");			
				String no_mapping 		= (String) map.get("no_mapping");
				int idbarang 			= (int) map.get("idbarang");
				String nama_barang 		= (String) map.get("nama_barang");
				int sparepart_id 		= (int) map.get("sparepart_id");
				String sparepart_nama 	= (String) map.get("sparepart_nama");
				String url 				= valueToStringOrEmptyString(map, "url");
				int qty 				= valueToIntOrZero(map, "qty");
				String serial_number 	= valueToStringOrEmptyString(map, "serial_number");
				int jenis_perangkat		= valueToIntOrZero(map, "jenis_perangkat");
				String kondisi_barang 	= valueToStringOrEmptyString(map, "kondisi_barang");
				String keterangan 		= valueToStringOrEmptyString(map, "keterangan");
				String deskripsi 		= valueToStringOrEmptyString(map, "deskripsi");
				int rekanan_id 			= valueToIntOrZero(map, "rekanan_id");
				String rekanan_nama 	= valueToStringOrEmptyString(map, "rekanan_nama");
				Date tgl_act 			= (Date) map.get("tgl_act");
				Date tgl_update 		= (Date) map.get("tgl_update");
				int status 				= valueToIntOrZero(map, "status");
				String ip_lan 			= valueToStringOrEmptyString(map, "ip_lan");
				String mac_lan 			= valueToStringOrEmptyString(map, "mac_lan");
				String ip_wifi 			= valueToStringOrEmptyString(map, "ip_wifi");
				String mac_wifi 		= valueToStringOrEmptyString(map, "mac_wifi");
				int id_vlan 			= valueToIntOrZero(map, "id_vlan");
				int id_switch 			= valueToIntOrZero(map, "id_switch");
				int status_online 		= valueToIntOrZero(map, "status_online");
				int user_id 			= valueToIntOrZero(map, "user_id");
				int sw_port 			= valueToIntOrZero(map, "sw_port");
				int status_layak 		= valueToIntOrZero(map, "status_layak");
				String user_nama 		= valueToStringOrEmptyString(map, "user_nama");
				
				
				
				jObj.addProperty("id", id); 			
				jObj.addProperty("no_mapping", no_mapping);	
				jObj.addProperty("idbarang", idbarang);	
				jObj.addProperty("nama_barang", nama_barang);
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("sparepart_nama", sparepart_nama);
				jObj.addProperty("url", url);
				jObj.addProperty("qty",	qty);	
				jObj.addProperty("serial_number", serial_number);
				jObj.addProperty("jenis_perangkat", jenis_perangkat);
				jObj.addProperty("kondisi_barang", kondisi_barang);
				jObj.addProperty("keterangan", 	keterangan);
				jObj.addProperty("deskripsi", deskripsi);	
				jObj.addProperty("rekanan_id",rekanan_id);	
				jObj.addProperty("rekanan_nama", rekanan_nama);
				jObj.addProperty("tgl_act", dateFormat.format(tgl_act));
				jObj.addProperty("tgl_update", dateFormat.format(tgl_update));
				jObj.addProperty("status", 	status);	
				jObj.addProperty("ip_lan", ip_lan);	
				jObj.addProperty("mac_lan", mac_lan);
				jObj.addProperty("ip_wifi", ip_wifi);
				jObj.addProperty("mac_wifi", mac_wifi);
				jObj.addProperty("id_vlan", id_vlan);
				jObj.addProperty("id_switch", id_switch);
				jObj.addProperty("status_online", status_online);
				jObj.addProperty("user_id", user_id	);
				jObj.addProperty("sw_port", sw_port);	
				jObj.addProperty("status_layak", status_layak);
				jObj.addProperty("user_nama", user_nama);
				
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String getSocketPrint() {
		SocketPrint sp = new SocketPrint();
		sp.writeMessage("mayudha-4");
		//byte [] result = sp.getRaster("mayudha-3");
		//sp.convertTextToGraphic("Hello World");
		sp.closeSocket();
//		String [] strText = {"1", "/", "0", "0", "0", "0", "9"};
//		String [] strText = {};
//		byte [] result = sp.generateQRCode("1/00009", 200, 200, strText);
//		String bytearay="";
		
//		for (byte b : result) {
//			
//		}
		
		return "print";//byteArrayToHex(result);
	}
	
	private String getTransaksiTonerHistory(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
//		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String tonerId			= params[1];
		
		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.kartu_stok_toner WHERE toner_id= :tonerId ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tonerId", tonerId)
					.executeAndFetchTable().asList();
			
			if(getReportData.size() == 0) {
				jResult.addProperty("status", 409);
				jResult.addProperty("additional_message", "Toner tidak ditemukan!");
				return jResult.toString();
			}
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				String referensi			= valueToStringOrEmptyString(map, "referensi");
				Date tgl_act				= (Date)map.get("tgl_act");
				int petugas_id				= valueToIntOrZero(map, "user_id");
				String keterangan			= valueToStringOrEmptyString(map, "keterangan");
				String toner_id				= valueToStringOrEmptyString(map, "toner_id");
//				int status					= valueToIntOrZero(map, "status");
				int penerimaanId			= valueToIntOrZero(map, "penerimaan_id");
				int jenisTrans				= valueToIntOrZero(map, "jenis_trans");
				int ruangKirim				= valueToIntOrZero(map, "unit_id_kirim");
				int ruangTerima				= valueToIntOrZero(map, "unit_id_terima");
				double qtyIn				= (double)map.get("qty_in");
				double qtyOut				= (double)map.get("qty_out");
				int ruangId					= valueToIntOrZero(map, "unit_id");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("referensi", referensi);
				jObj.addProperty("keterangan", keterangan);
				jObj.addProperty("petugas_id", petugas_id);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("toner_id", toner_id);
//				jObj.addProperty("status", status);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("jenis_trans_text", jenisTransaksi(jenisTrans));
				jObj.addProperty("qty_in", qtyIn);
				jObj.addProperty("qty_out", qtyOut);
				jObj.addProperty("ruang_id", ruangId);
				
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<11; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "Penerimaan");
						jArrStat.add(jObj);
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "Pemakaian");		
						jArrStat.add(jObj);
						break;
//					case 3:
//						jObj.addProperty("value", 3);
//						jObj.addProperty("text", "kirim");			
//						break;
//					case 4:
//						jObj.addProperty("value", 4);
//						jObj.addProperty("text", "terima");			
//						break;
					case 5:
						jObj.addProperty("value", 5);
						jObj.addProperty("text", "retur unit");
						jArrStat.add(jObj);
						break;
					case 6:
						jObj.addProperty("value", 6);
						jObj.addProperty("text", "retur pihak ke 3");
						jArrStat.add(jObj);
						break;
//					case 7:
//						jObj.addProperty("value", 7);
//						jObj.addProperty("text", "batal pemakaian");			
//						break;
					case 8:
						jObj.addProperty("value", 8);
						jObj.addProperty("text", "Purchase Order");
						jArrStat.add(jObj);
						break;
					case 9:
						jObj.addProperty("value", 8);
						jObj.addProperty("text", "Batal Purchase Order");
						jArrStat.add(jObj);
						break;
					case 10:
						jObj.addProperty("value", 10);
						jObj.addProperty("text", "stok opname");
						jArrStat.add(jObj);
						break;
						
					default:
						break;
				}
				
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			jResult.add("list_jenis_trans", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	
	private String getTransaksiTonerPengeluaranBarangUser(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
//		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}
//
//		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select pb.*, ks.part_tambahan FROM transaksi.toner_pengeluaran_barang_detail pb inner join transaksi.kartu_stok_toner ks on pb.penerimaan_id = ks.penerimaan_id WHERE pb.user_id= :id_user AND "
					+ "pb.tgl_act >= :tglAwal AND pb.tgl_act < :tglAkhir ORDER BY pb.id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("id_user", id_user)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String noPengeluaran		= valueToStringOrEmptyString(map, "referensi");
				Date tgl_act				= (Date)map.get("tgl_act");
				int petugas_id				= valueToIntOrZero(map, "user_id");
				String petugas_nama			= valueToStringOrEmptyString(map, "user_nama");
				String toner_id				= valueToStringOrEmptyString(map, "toner_id");
				String toner_nama			= valueToStringOrEmptyString(map, "toner_nama");
				int status					= valueToIntOrZero(map, "status");
				int penerimaanId			= valueToIntOrZero(map, "penerimaan_id");
				int vendorId				= valueToIntOrZero(map, "vendor_id");
				
				Gson gson = new Gson();
				JsonArray jsonArrTambahan = new JsonArray();
				try {
					PGobject pgObject = (PGobject) map.get("part_tambahan");

				    if (pgObject == null) {
//				        return null;
				    }
				    else {
					    String type = pgObject.getType();
					    String partTambahan = pgObject.getValue();
						jsonArrTambahan = gson.fromJson(partTambahan, JsonArray.class);
	//					System.out.println(value);
				    }
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_pengeluaran", noPengeluaran);
				jObj.addProperty("petugas_id", petugas_id);
				jObj.addProperty("petugas_nama", petugas_nama);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("toner_id", toner_id);
				jObj.addProperty("toner_nama", toner_nama);
				jObj.addProperty("status", status);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.add("part_tambahan", jsonArrTambahan);
				jObj.addProperty("bukacollapse", false);
				
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<6; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "Dibawa");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "Rusak");			
						break;
					case 3:
						jObj.addProperty("value", 3);
						jObj.addProperty("text", "Dipasang");			
						break;
					case 4:
						jObj.addProperty("value", 4);
						jObj.addProperty("text", "Kembali");			
						break;
					case 5:
						jObj.addProperty("value", 5);
						jObj.addProperty("text", "");			
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_pengeluaran", jArr);
			jResult.add("list_pengeluaran_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	
	private String getTransaksiTonerPOTersedia2(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
				
		String user_id			= req.params(":id");
		int id_user				= Integer.parseInt(user_id);
		
		JsonObject jResult = new JsonObject();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "SELECT a.toner_nama, a.sparepart_id, a.serial_number, a.warna, a.originalitas FROM master.ms_toner a "
					+ "where serial_number not in (select toner_id from transaksi.toner_po_detail where status=1)";
			
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr = new JsonArray();
							
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj = new JsonObject();
				String toner_nama 		= (String) map.get("toner_nama");
				int sparepart_id 		= (int) map.get("sparepart_id");
				String serial_number 	= (String) map.get("serial_number");
				String warna 			= (String) map.get("warna");
				int originalitas 		= (int) map.get("originalitas");
//				String str_toner_id		= (String) map.get("toner_id");
				jObj.addProperty("toner_nama", toner_nama);
				jObj.addProperty("sparepart_id", sparepart_id);
				jObj.addProperty("serial_number", serial_number);
				jObj.addProperty("warna", warna);
				jObj.addProperty("originalitas", originalitas);
				
//				jObj.addProperty("toner_id", str_toner_id);
				jObj.addProperty("value", toner_nama);
				jObj.addProperty("label", toner_nama);
				jarr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("po_toner_detail", jarr);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPOTersedia(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
				
		String user_id			= req.params(":id");
		int id_user				= Integer.parseInt(user_id);
		
		JsonObject jResult = new JsonObject();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "SELECT id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa, verif_retur, status_keluar "
					+ "FROM ( SELECT DISTINCT ON (penerimaan_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, "
					+ "vendor_id, stok_sisa, verif_retur, status_keluar, rank() "
					+ "OVER (PARTITION BY penerimaan_id ORDER BY id DESC) AS r "
					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = 510) AS dt WHERE r = 1 and "
					+ "jenis_trans = 5 AND verif_retur != -1 AND status_keluar = 0 AND stok_sisa > 0 ORDER BY id desc";
					
//					"SELECT  a.toner_nama, a.sparepart_id, a.serial_number, a.warna, a.originalitas FROM master.ms_toner a "
//					+ "where serial_number not in (select toner_id from transaksi.toner_po_detail where status=1)";
			
//			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jArr = new JsonArray();
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
//					.addParameter("unitTujuan", unitTujuan)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int jenisTrans				= (int)map.get("jenis_trans");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= (int)map.get("vendor_id");
				int verifRetur				= (int)map.get("verif_retur");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("verif_retur", verifRetur);
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_toner_ready", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getTransaksiTonerPoReturTersedia(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
				
		String user_id			= req.params(":id");
		int id_user				= Integer.parseInt(user_id);
		
		JsonObject jResult = new JsonObject();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
//		
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery= "SELECT id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa, verif_retur, status_keluar "
					+ "FROM ( SELECT DISTINCT ON (penerimaan_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, "
					+ "vendor_id, stok_sisa, verif_retur, status_keluar, rank() "
					+ "OVER (PARTITION BY penerimaan_id ORDER BY id DESC) AS r "
					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = 510) AS dt WHERE r = 1 and "
					+ "jenis_trans in (5, 8, 9, 10) AND verif_retur != -1 AND status_keluar != 1 AND stok_sisa > 0 ORDER BY id desc";

			JsonArray jArr = new JsonArray();
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
//					.addParameter("unitTujuan", unitTujuan)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int jenisTrans				= (int)map.get("jenis_trans");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= (int)map.get("vendor_id");
				int verifRetur				= (int)map.get("verif_retur");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("nilai", verifRetur);
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
			
			sqlquery	= "SELECT * FROM transaksi.toner_rusak where status_keluar = 0";
			getReportData = conn.createQuery(sqlquery)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= valueToIntOrZero(map, "vendor_id");
				String keterangan			= valueToStringOrEmptyString(map, "keterangan");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", 5); // sebenarnya bukan pengembalian (5), tapi toner rusak sebelum di input ke kartu stok pemasangan
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("keterangan", keterangan);
				jObj.addProperty("nilai", 1); // retur
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
			
//			sqlquery	= "SELECT id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa, verif_retur, status_keluar "
//					+ "FROM ( SELECT DISTINCT ON (penerimaan_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, "
//					+ "vendor_id, stok_sisa, verif_retur, status_keluar, rank() "
//					+ "OVER (PARTITION BY penerimaan_id ORDER BY id DESC) AS r "
//					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = 510) AS dt WHERE r = 1 and "
//					+ "jenis_trans = 10 AND status_keluar != 1 AND stok_sisa > 0 ORDER BY id desc";
//			getReportData = conn.createQuery(sqlquery)
//					.executeAndFetchTable().asList();
//			
//			for (Map<String, Object> map : getReportData) {
//				long id						= (long)map.get("id");
//				int penerimaanId			= (int)map.get("penerimaan_id");
//				String tonerId 				= (String)map.get("toner_id");
//				int vendorId				= valueToIntOrZero(map, "vendor_id");
//				
//				JsonObject jObj				= new JsonObject();
//				jObj.addProperty("id", id);
//				jObj.addProperty("penerimaan_id", penerimaanId);
//				jObj.addProperty("jenis_trans", 5); // sebenarnya bukan pengembalian (5), tapi toner rusak sebelum di input ke kartu stok pemasangan
//				jObj.addProperty("toner_id", tonerId);
//				jObj.addProperty("id_pihak_ke3", vendorId);
//				jObj.addProperty("nilai", 1); // retur
//				jObj.addProperty("label", tonerId);
//				jObj.addProperty("value", tonerId);
//				jArr.add(jObj);
//			
//			}
			
						
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPODetail(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String noPO				= params[1];
		
		System.out.println("Params: " + noPO);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery	= "select distinct(toner_id) as tn_id,* from (select pod.*, pd.invoice FROM transaksi.toner_po_detail pod left join transaksi.toner_penerimaan_detail pd on pod.no_po = pd.no_po and pod.toner_id = pd.toner_id WHERE pod.no_po = :noPO ) t";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("noPO", noPO)
					.executeAndFetchTable().asList();
			
			JsonArray jArr	= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj		= new JsonObject();
				int idDetail 		= (int)map.get("id_detail");
				String tonerId 		= (String)map.get("toner_id");
				String tonerNama 	= (String)map.get("toner_nama");
				int status 			= (int)map.get("status");
				Date tgl_act		= (Date)map.get("tgl_act");
				String invoice		= valueToStringOrEmptyString(map, "invoice");
				
				jObj.addProperty("id_detail", idDetail);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("toner_nama", tonerNama);
				jObj.addProperty("status", status);
				jObj.addProperty("invoice", invoice);
				jObj.addProperty("tgl_po", dateFormat.format(tgl_act));
				jArr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_po_toner_detail", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPOList(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int status_po			= Integer.parseInt(params[1]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}

		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.toner_po_header WHERE tgl_act >= :tglAwal AND tgl_act < :tglAkhir ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String no_po 				= (String)map.get("no_po");
				int idPihak3				= (int)map.get("id_pihak_ketiga");
				String nama_pihak_ketiga 	= (String)map.get("nama_pihak_ketiga");
				int status 					= valueToIntOrZero(map, "status_po");
				Date tgl_act				= (Date)map.get("tgl_act");
				int ketBatal				= valueToIntOrZero(map, "ket_batal");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_po", no_po);
				jObj.addProperty("id_pihak_ketiga", idPihak3);
				jObj.addProperty("nama_pihak_ketiga", nama_pihak_ketiga);
				jObj.addProperty("status", status);
				jObj.addProperty("tgl_po", dateTimeFormat.format(tgl_act));
				jObj.addProperty("ket_batal", ketBatal);
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<5; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "normal");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "batal");			
						break;
					case 3:
						jObj.addProperty("value", 3);
						jObj.addProperty("text", "diterima semua");
						break;
					case 4:
						jObj.addProperty("value", 4);
						jObj.addProperty("text", "diterima sebagian");
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_po_toner", jArr);
			jResult.add("list_po_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturDetail(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String noRetur			= params[1];
		
		System.out.println("Params: " + noRetur);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery	= "select * FROM transaksi.retur_detail WHERE no_retur = :noRetur";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("noRetur", noRetur)
					.executeAndFetchTable().asList();
			
			JsonArray jArr	= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj		= new JsonObject();
				int idDetail 		= (int)map.get("id_detail");
				String tonerId 		= (String)map.get("toner_id");
				String tonerNama 	= (String)map.get("toner_nama");
				int status 			= (int)map.get("status");
				Date tgl_act		= (Date)map.get("tgl_act");
				int penerimaan_id	= (int)map.get("penerimaan_id");
				
				jObj.addProperty("id_detail", idDetail);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("toner_nama", tonerNama);
				jObj.addProperty("status", status);
				jObj.addProperty("tgl_act", dateFormat.format(tgl_act));
				jObj.addProperty("penerimaan_id", penerimaan_id);
				jArr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturList(Sql2o sql2o, Request req) {
		
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int status_po			= Integer.parseInt(params[1]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}

		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.retur_header WHERE tgl_act >= :tglAwal AND tgl_act < :tglAkhir ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String no_po 				= (String)map.get("no_retur");
				int idPihak3				= (int)map.get("id_pihak_ketiga");
				String nama_pihak_ketiga 	= (String)map.get("nama_pihak_ketiga");
				int status 					= valueToIntOrZero(map, "status");
				Date tgl_act				= (Date)map.get("tgl_act");
				int userId					= valueToIntOrZero(map, "user_id");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_po", no_po);
				jObj.addProperty("id_pihak_ketiga", idPihak3);
				jObj.addProperty("nama_pihak_ketiga", nama_pihak_ketiga);
				jObj.addProperty("status", status);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("user_id", userId);
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<5; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "normal");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "batal");			
						break;
					case 3:
						jObj.addProperty("value", 3);
						jObj.addProperty("text", "diterima semua");
						break;
					case 4:
						jObj.addProperty("value", 4);
						jObj.addProperty("text", "diterima sebagian");
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			jResult.add("list_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturPenerimaanInv(Sql2o sql2o, Request req) {

//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String  invoice			= params[1];
		
				
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.toner_penerimaan_detail where invoice= :invoice AND is_retur= 1 ORDER BY id_detail DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("invoice", invoice)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id_detail");
				String inv 					= valueToStringOrEmptyString(map, "invoice");
				String tonerId				= (String)map.get("toner_id");
				Date tglAct					= (Date)map.get("tgl_act");
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("invoice", invoice);
				jObj.addProperty("tonerId", tonerId);
				jObj.addProperty("tglAct", dateFormat.format(tglAct));
				jArr.add(jObj);
			
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturPenerimaanListInv(Sql2o sql2o, Request req) {

//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int status_po			= Integer.parseInt(params[1]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}

		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "SELECT * FROM (SELECT DISTINCT ON (invoice) invoice, id_detail, tgl_act FROM transaksi.toner_penerimaan_detail WHERE tgl_act >= :tglAwal AND tgl_act < :tglAkhir ) t order by id_detail desc";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id_detail");
				String invoice 				= valueToStringOrEmptyString(map, "invoice");
				Date tglAct					= (Date)map.get("tgl_act");
				if(invoice.length()>0) {
					JsonObject jObj				= new JsonObject();
					jObj.addProperty("id", id);
					jObj.addProperty("invoice", invoice);
					jObj.addProperty("tgl_act", dateFormat.format((tglAct)));
					jArr.add(jObj);
				}
			
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturTersediaVendor(Sql2o sql2o, Request req) {
		
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int status_po			= Integer.parseInt(params[1]);
		
		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
//			String sqlquery	= "SELECT rd.*, rh.id_pihak_ketiga, rh.nama_pihak_ketiga FROM transaksi.retur_detail as rd INNER JOIN transaksi.retur_header rh ON rd.no_retur = rh.no_retur WHERE rd.status = 1";
			String sqlquery	= "SELECT distinct rh.id_pihak_ketiga, rh.nama_pihak_ketiga FROM transaksi.retur_detail as rd INNER JOIN transaksi.retur_header rh ON rd.no_retur = rh.no_retur WHERE rd.status = 1";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				
				int idPihak3				= (int)map.get("id_pihak_ketiga");
				String nama_pihak_ketiga 	= (String)map.get("nama_pihak_ketiga");
				
				JsonObject jObj				= new JsonObject();

				jObj.addProperty("id_pihak_ketiga", idPihak3);
				jObj.addProperty("nama_pihak_ketiga", nama_pihak_ketiga);

				jArr.add(jObj);
			
			}
									
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturTersediaToner(Sql2o sql2o, Request req) {
		
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int idVendor			= Integer.parseInt(params[1]);
		
		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			// filter berdasarkan vendor
//			String sqlquery	= "SELECT rd.*, rh.id_pihak_ketiga, rh.nama_pihak_ketiga FROM transaksi.retur_detail as rd INNER JOIN transaksi.retur_header rh ON rd.no_retur = rh.no_retur WHERE rd.status = 1 AND id_pihak_ketiga =:idVendor";
			String sqlquery	= "SELECT rd.*, rh.id_pihak_ketiga, rh.nama_pihak_ketiga FROM transaksi.retur_detail as rd INNER JOIN transaksi.retur_header rh ON rd.no_retur = rh.no_retur WHERE rd.status = 1";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
//					.addParameter("idVendor", idVendor)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				
//				int id						= (int)map.get("id");
				String no_retur				= (String)map.get("no_retur");
				int idPihak3				= (int)map.get("id_pihak_ketiga");
				String tonerId				= (String)map.get("toner_id");
				String nama_pihak_ketiga 	= (String)map.get("nama_pihak_ketiga");
				int status 					= valueToIntOrZero(map, "status");
				Date tgl_act				= (Date)map.get("tgl_act");
				int userId					= valueToIntOrZero(map, "user_id");
				int penerimaanId			= valueToIntOrZero(map, "penerimaan_id");
				
				JsonObject jObj				= new JsonObject();
//				jObj.addProperty("id", id);
				jObj.addProperty("no_retur", no_retur);
				jObj.addProperty("id_pihak_ketiga", idPihak3);
				jObj.addProperty("nama_pihak_ketiga", nama_pihak_ketiga);
				jObj.addProperty("status", status);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("user_id", userId);
				jObj.addProperty("penerimaan_id", penerimaanId);

				jArr.add(jObj);
			
			}
									
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	
	private String getTransaksiTonerPOPenerimaanDetail(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String noPenerimaan		= params[1];
		
		System.out.println("Params: " + noPenerimaan);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery	= "select * FROM transaksi.toner_penerimaan_detail WHERE no_penerimaan = :noPenerimaan";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("noPenerimaan", noPenerimaan)
					.executeAndFetchTable().asList();
			
			JsonArray jArr	= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj		= new JsonObject();
				int idDetail 		= (int)map.get("id_detail");
				String noPO			= valueToStringOrEmptyString(map, "no_po");
				String tonerId 		= (String)map.get("toner_id");
				String tonerNama 	= (String)map.get("toner_nama");
				int status 			= (int)map.get("status");
				Date tgl_act		= (Date)map.get("tgl_act");
				String invoice		= valueToStringOrEmptyString(map, "invoice");
				
				jObj.addProperty("id_detail", idDetail);
				jObj.addProperty("no_po", noPO);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("toner_nama", tonerNama);
				jObj.addProperty("status", status);
				jObj.addProperty("invoice", invoice);
				jObj.addProperty("tgl_po", dateFormat.format(tgl_act));
				jArr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_po_toner_detail", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getTransaksiTonerPOPenerimaanList(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int status_po			= Integer.parseInt(params[1]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}

		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.toner_penerimaan_header WHERE tgl_act >= :tglAwal AND tgl_act < :tglAkhir ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String no_po 				= valueToStringOrEmptyString(map, "no_po");
				String no_penerimaan		= valueToStringOrEmptyString(map, "no_penerimaan");
				int idPihak3				= valueToIntOrZero(map, "id_pihak_ketiga");
				String nama_pihak_ketiga 	= (String)map.get("nama_pihak_ketiga");
				int status 					= valueToIntOrZero(map, "status_penerimaan");
				Date tgl_act				= (Date)map.get("tgl_act");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_po", no_po);
				jObj.addProperty("no_penerimaan", no_penerimaan);
				jObj.addProperty("id_pihak_ketiga", idPihak3);
				jObj.addProperty("nama_pihak_ketiga", nama_pihak_ketiga);
				jObj.addProperty("status", status);
				jObj.addProperty("tgl_po", dateTimeFormat.format(tgl_act));
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<3; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "diterima");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "batal");			
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_penerimaan_toner", jArr);
			jResult.add("list_penerimaan_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPemakaianDetail(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		String noPemakaian		= params[1];
		
		System.out.println("Params: " + noPemakaian);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery	= "select * FROM transaksi.pemakaian_unit_detail WHERE no_pemakaian = :noPemakaian";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("noPemakaian", noPemakaian)
					.executeAndFetchTable().asList();
			
			JsonArray jArr	= new JsonArray();
			for (Map<String, Object> map : getReportData) {
				JsonObject jObj		= new JsonObject();
				int idDetail 		= (int)map.get("id");
				String noPK			= valueToStringOrEmptyString(map, "no_pemakaian");
				String tonerId 		= (String)map.get("toner_id");
				String tonerNama 	= (String)map.get("toner_nama");
				int status 			= (int)map.get("status");
				
				jObj.addProperty("id_detail", idDetail);
				jObj.addProperty("no_po", noPK);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("toner_nama", tonerNama);
				jObj.addProperty("status", status);
				jArr.add(jObj);
			}
			
			jResult.addProperty("status", 200);
			jResult.add("data_po_toner_detail", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getTransaksiTonerPemakaianList(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
//		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}
//
//		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.pemakaian_unit_header WHERE user_id=:id_user AND tgl_act >= :tglAwal AND tgl_act < :tglAkhir ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("id_user", id_user)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String noPemakaian			= valueToStringOrEmptyString(map, "no_pemakaian");
				int ruanganId				= (int)map.get("unit_id");
				String ruanganNama 			= (String)map.get("unit_nama");
				Date tgl_act				= (Date)map.get("tgl_act");
				int petugas_id				= valueToIntOrZero(map, "user_id");
				String petugas_nama			= valueToStringOrEmptyString(map, "user_nama");
				int user_pemakai			= valueToIntOrZero(map, "user_pemakai");
				String nama_pemakai			= valueToStringOrEmptyString(map, "nama_pemakai");
				int userIdBatal				= valueToIntOrZero(map, "user_id_batal");
				int ketBatal				= valueToIntOrZero(map, "ket_batal");
				String tglBatal= "";
				
				if(userIdBatal > 0) 
					tglBatal			=  dateTimeFormat.format((Date)map.get("tgl_batal"));
							
				
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_pemakaian", noPemakaian);
				jObj.addProperty("ruangan_id", ruanganId);
				jObj.addProperty("ruangan_nama", ruanganNama);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("petugas_id", petugas_id);
				jObj.addProperty("petugas_nama", petugas_nama);
				jObj.addProperty("user_pemakai", user_pemakai);
				jObj.addProperty("nama_pemakai", nama_pemakai);
				jObj.addProperty("user_id_batal", userIdBatal);
				jObj.addProperty("tgl_batal", tglBatal);
				jObj.addProperty("ket_batal", ketBatal);
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<4; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "Toner Rusak");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "Salah Entry");			
						break;
					case 3:
						jObj.addProperty("value", 3);
						jObj.addProperty("text", "Lain - Lain");			
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_pemakaian_toner", jArr);
			jResult.add("list_batal_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPemakaianListAllUser(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
//		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}
//
//		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.pemakaian_unit_header WHERE tgl_act >= :tglAwal AND tgl_act < :tglAkhir ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String noPemakaian			= valueToStringOrEmptyString(map, "no_pemakaian");
				int ruanganId				= (int)map.get("unit_id");
				String ruanganNama 			= (String)map.get("unit_nama");
				Date tgl_act				= (Date)map.get("tgl_act");
				int petugas_id				= valueToIntOrZero(map, "user_id");
				String petugas_nama			= valueToStringOrEmptyString(map, "user_nama");
				int user_pemakai			= valueToIntOrZero(map, "user_pemakai");
				String nama_pemakai			= valueToStringOrEmptyString(map, "nama_pemakai");
				int userIdBatal				= valueToIntOrZero(map, "user_id_batal");
				int ketBatal				= valueToIntOrZero(map, "ket_batal");
				String tglBatal= "";
				
				if(userIdBatal > 0) 
					tglBatal			=  dateTimeFormat.format((Date)map.get("tgl_batal"));
							
				
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_pemakaian", noPemakaian);
				jObj.addProperty("ruangan_id", ruanganId);
				jObj.addProperty("ruangan_nama", ruanganNama);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("petugas_id", petugas_id);
				jObj.addProperty("petugas_nama", petugas_nama);
				jObj.addProperty("user_pemakai", user_pemakai);
				jObj.addProperty("nama_pemakai", nama_pemakai);
				jObj.addProperty("user_id_batal", userIdBatal);
				jObj.addProperty("tgl_batal", tglBatal);
				jObj.addProperty("ket_batal", ketBatal);
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<4; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "Toner Rusak");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "Salah Entry");			
						break;
					case 3:
						jObj.addProperty("value", 3);
						jObj.addProperty("text", "Lain - Lain");			
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_pemakaian_toner", jArr);
			jResult.add("list_batal_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPengembalianList(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
//		
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		
		Date tglAwal;
		Date tglAkhir;
		
		try {
			tglAwal					= dateFormat.parse(params[1]);
			tglAkhir				= dateTimeFormat.parse(params[2]+" 23:59:59");
		}
		catch (Exception e) {
			e.printStackTrace();
			
			tglAkhir				= new Date();
			// Create a calendar object with today date. Calendar is in java.util pakage.
		    Calendar calendar = Calendar.getInstance();

		    // Move calendar to yesterday
		    calendar.add(Calendar.DATE, -1);
			tglAwal					= calendar.getTime();
			
		}
//
//		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		System.out.println("tglAwal: " + dateFormat.format(tglAwal) + " tglAkhir: " + dateFormat.format(tglAkhir));
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrStat 	= new JsonArray();
			
			String sqlquery	= "select * FROM transaksi.pengembalian_unit_header WHERE tgl_act >= :tglAwal AND tgl_act < :tglAkhir ORDER BY id DESC";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("tglAwal", tglAwal)
					.addParameter("tglAkhir", tglAkhir)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String noPemakaian			= valueToStringOrEmptyString(map, "no_pengembalian");
				int ruanganId				= (int)map.get("unit_id");
				String ruanganNama 			= (String)map.get("unit_nama");
				Date tgl_act				= (Date)map.get("tgl_act");
				int petugas_id				= valueToIntOrZero(map, "user_id");
				String petugas_nama			= valueToStringOrEmptyString(map, "user_nama");
				int user_pemakai			= valueToIntOrZero(map, "user_pemakai");
				String nama_pemakai			= valueToStringOrEmptyString(map, "nama_pemakai");
				int userIdBatal				= valueToIntOrZero(map, "user_id_batal");
				int ketBatal				= valueToIntOrZero(map, "ket_batal");
				String tglBatal= "";
				
				if(userIdBatal > 0) 
					tglBatal			=  dateTimeFormat.format((Date)map.get("tgl_batal"));
							
				
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("no_pemakaian", noPemakaian);
				jObj.addProperty("ruangan_id", ruanganId);
				jObj.addProperty("ruangan_nama", ruanganNama);
				jObj.addProperty("tgl_act", dateTimeFormat.format(tgl_act));
				jObj.addProperty("petugas_id", petugas_id);
				jObj.addProperty("petugas_nama", petugas_nama);
				jObj.addProperty("user_pemakai", user_pemakai);
				jObj.addProperty("nama_pemakai", nama_pemakai);
				jObj.addProperty("user_id_batal", userIdBatal);
				jObj.addProperty("tgl_batal", tglBatal);
				jObj.addProperty("ket_batal", ketBatal);
				jArr.add(jObj);
			
			}
			
			
			for(int i=1; i<4; i++) {
				JsonObject jObj				= new JsonObject();
				switch (i) {
					case 1:
						jObj.addProperty("value", 1);
						jObj.addProperty("text", "Toner Rusak");
						break;
					case 2:
						jObj.addProperty("value", 2);
						jObj.addProperty("text", "Salah Entry");			
						break;
					case 3:
						jObj.addProperty("value", 3);
						jObj.addProperty("text", "Lain - Lain");			
						break;
					default:
						break;
				}
				jArrStat.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_pengembalian_toner", jArr);
			jResult.add("list_batal_status", jArrStat);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerPOPenerimaanListKomponenTambahan(Sql2o sql2o) {
		
		JsonObject jResult = new JsonObject();
		
		try(Connection connSql= sql2o.open()){
			
			String sqlquery= "SELECT * FROM master.ms_toner_tambahan_perbaikan WHERE status = 1";
			List<Map<String,Object>> getReportData = connSql.createQuery(sqlquery).executeAndFetchTable().asList();
			
			JsonArray jarr 	= new JsonArray();
			int i			= 0;	
			
			for (Map<String, Object> map : getReportData) {
								
				JsonObject jObj = new JsonObject();
				
				int id 			= (int) map.get("id");
				String nama 	= (String) map.get("nama");
				
				jObj.addProperty("id", id);
				jObj.addProperty("nama", nama);
				jObj.addProperty("value", "");
				jarr.add(jObj);
			}
			jResult.addProperty("status", 200);
			jResult.add("data", jarr);
			return jResult.toString();
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getTransaksiTonerPemakaianTersedia(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
				 
		String user_id			= req.params(":id");
		int id_user				= Integer.parseInt(user_id);
		
				
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
//		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			
			String sqlquery	= "SELECT id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa, part_tambahan, is_rusak FROM ( "
					+ "SELECT DISTINCT ON (toner_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, "
					+ "stok_sisa, part_tambahan, is_rusak, rank() OVER (PARTITION BY toner_id ORDER BY id DESC) AS r "
					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = 510 and toner_id not in (select toner_id "
					+ "from transaksi.toner_pengeluaran_barang_detail where status = 1 )) AS dt "
					+ "WHERE r = 1 and jenis_trans =1 AND stok_sisa > 0 AND is_rusak != 1 ORDER BY id desc";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int jenisTrans				= (int)map.get("jenis_trans");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= (int)map.get("vendor_id");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_toner_ready", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String getTransaksiTonerPengembalianTersedia(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
				 
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
		int unitTujuan			= Integer.parseInt(params[1]);
		
				
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr	= new JsonArray();
			
			String sqlquery	= "SELECT id, penerimaan_id, jenis_trans,qty_in, toner_id, tgl_trans, vendor_id, stok_sisa "
					+ "FROM ( SELECT DISTINCT ON (penerimaan_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, tgl_trans, "
					+ "vendor_id, stok_sisa, rank() "
					+ "OVER (PARTITION BY penerimaan_id ORDER BY id DESC) AS r "
					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = :unitTujuan) AS dt WHERE r = 1 and "
					+ "jenis_trans =2 AND stok_sisa > 0 ORDER BY id desc";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("unitTujuan", unitTujuan)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int jenisTrans				= (int)map.get("jenis_trans");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= (int)map.get("vendor_id");
				Date tglTrans				= (Date)map.get("tgl_trans");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("tgl_trans", dateFormat.format(tglTrans));
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_toner_ready", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerReturPihak3Tersedia(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
				 
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int unitTujuan			= Integer.parseInt(params[1]);
		
				
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
//		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr	= new JsonArray();
			
			String sqlquery	= "SELECT id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa FROM ( "
					+ "SELECT DISTINCT ON (penerimaan_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, "
					+ "stok_sisa, rank() OVER (PARTITION BY penerimaan_id ORDER BY id DESC) AS r "
					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = 510 and toner_id in (select toner_id "
					+ "from transaksi.toner_pengeluaran_barang_detail where status = 2 )) AS dt "
					+ "WHERE r = 1 and jenis_trans =1 AND stok_sisa > 0 ORDER BY id desc";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int jenisTrans				= (int)map.get("jenis_trans");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= (int)map.get("vendor_id");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data_toner_ready", jArr);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String getTransaksiTonerVerifikasiReturTersedia(Sql2o sql2o, Request req) {
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
				 
		String[] params			= req.splat();
		 
		int id_user				= Integer.parseInt(params[0]);
//		int unitTujuan			= Integer.parseInt(params[1]);
		
				
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
//		
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			JsonArray jArr		= new JsonArray();
			JsonArray jArrRetur	= new JsonArray();
			
			String sqlquery	= "SELECT dt.id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa, verif_retur, referensi, user_id, u.nama "
					+ "FROM (SELECT DISTINCT ON (penerimaan_id) id, penerimaan_id, jenis_trans,qty_in, toner_id, vendor_id, stok_sisa, "
					+ "verif_retur, referensi, user_id, rank() OVER (PARTITION BY penerimaan_id ORDER BY id DESC) AS r "
					+ "FROM transaksi.kartu_stok_toner WHERE unit_id = 510) AS dt inner join master.ms_user u on dt.user_id= u.pegawai_id "
					+ "WHERE r = 1 and jenis_trans =5 AND verif_retur= -1 AND stok_sisa > 0 ORDER BY id desc";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				long id						= (long)map.get("id");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int jenisTrans				= (int)map.get("jenis_trans");
				String tonerId 				= (String)map.get("toner_id");
				int vendorId				= (int)map.get("vendor_id");
				String noPengembalian		= (String)map.get("referensi");
				int userId					= (int)map.get("user_id");
				String nama					= (String)map.get("nama");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("jenis_trans", jenisTrans);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("id_pihak_ke3", vendorId);
				jObj.addProperty("no_pengembalian", noPengembalian);
				jObj.addProperty("nilai", 0);
				jObj.addProperty("user_id", userId);
				jObj.addProperty("user_nama", nama);
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArr.add(jObj);
			
			}
			
			sqlquery = "select * from transaksi.toner_rusak where status_keluar = 0";
			getReportData = conn.createQuery(sqlquery)
					.executeAndFetchTable().asList();
			
			for (Map<String, Object> map : getReportData) {
				int id						= (int)map.get("id");
				String tonerId 				= (String)map.get("toner_id");
				Date tgl_act				= (Date)map.get("tgl_act");
				int userId					= (int)map.get("user_id");
				String userNama				= (String)map.get("user_nama");
				int penerimaanId			= (int)map.get("penerimaan_id");
				int statusKeluar			= (int)map.get("status_keluar");
				int vendorId				= (int)map.get("vendor_id");
				String keterangan			= valueToStringOrEmptyString(map, "keterangan");
				
				JsonObject jObj				= new JsonObject();
				jObj.addProperty("id", id);
				jObj.addProperty("toner_id", tonerId);
				jObj.addProperty("tgl_act", dateFormat.format(tgl_act));
				jObj.addProperty("user_id", userId);
				jObj.addProperty("user_nama", userNama);
				jObj.addProperty("penerimaan_id", penerimaanId);
				jObj.addProperty("keterangan", keterangan);
				jObj.addProperty("status_keluar", statusKeluar);
				
				jObj.addProperty("id_pihak_ke3", vendorId);
				
				jObj.addProperty("label", tonerId);
				jObj.addProperty("value", tonerId);
				jArrRetur.add(jObj);
			}
						
			jResult.addProperty("status", 200);
			jResult.add("data", jArr);
			jResult.add("data_retur", jArrRetur);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	public String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x ", b));
		   return sb.toString();
		}
	
	
	private String postInsertDetail(Sql2o sql2o, String jenisDetailNama, String idJenis, String idJenisNama) {

		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		try(Connection conn = sql2o.open()){
			
			String sqlquery							= "select * FROM master.ms_jenis_detail WHERE jenis_detail_nama = :jenis_detail_nama";
			List<Map<String,Object>> getReportData 	= conn.createQuery(sqlquery).addParameter("jenis_detail_nama", jenisDetailNama)
				.executeAndFetchTable().asList();
			
			int jenisCount 			= getReportData.size();
			if(jenisCount == 0) {
				sqlquery 			= "INSERT INTO master.ms_jenis_detail (jenis_detail_nama, idjenis, idjenis_nama, aktif) "
					+ "VALUES (:jenis_detail_nama, :idjenis, :idjenis_nama, :aktif)";
				
				int idJenisDetail 	= (int)conn.createQuery(sqlquery)
					.addParameter("jenis_detail_nama", jenisDetailNama)
					.addParameter("idjenis", idJenis)
					.addParameter("idjenis_nama", idJenisNama)
					.addParameter("aktif", aktifStat)
					.executeUpdate()
					.getKey();
					conn.commit();
				
				sqlquery 			= "select * FROM master.ms_jenis_detail WHERE jenis_detail_id = :idJenisDetail";
				getReportData 		= conn.createQuery(sqlquery)
					.addParameter("idJenisDetail", idJenisDetail)
					.executeAndFetchTable().asList();
				
				JsonObject jObj		= new JsonObject();
				int intVal 			= (int) getReportData.get(0).get("jenis_detail_id");
				String strVal		= (String) getReportData.get(0).get("jenis_detail_nama");
				String strVal1		= (String) getReportData.get(0).get("idjenis");
				String strVal2		= (String) getReportData.get(0).get("idjenis_nama");
				jObj.addProperty("jenis_detail_id", intVal);
				jObj.addProperty("jenis_detail_nama", strVal);
				jObj.addProperty("idjenis", strVal1);
				jObj.addProperty("idjenis_nama", strVal2);
				
				jResult.addProperty("status", 200);
				jResult.add("data_JenisDetail", jObj);
				
			}
			else {
				jResult.addProperty("status", 305);
			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postInsertJenis(Sql2o sql2o, String jenis) {

		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select * FROM master.ms_jenis WHERE jenis = :jenis";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("jenis", jenis)
					.executeAndFetchTable().asList();
			
			int jenisCount 	= getReportData.size();
			if(jenisCount == 0) {
				sqlquery 	= "INSERT INTO master.ms_jenis (jenis, aktif) VALUES (:jenis, :aktif)";
				
				int idjenis = (int)conn.createQuery(sqlquery)
				.addParameter("jenis", jenis)
				.addParameter("aktif", aktifStat)
				.executeUpdate()
				.getKey();
				conn.commit();
				
				sqlquery 	= "select * FROM master.ms_jenis WHERE idjenis = :idjenis";
				getReportData = conn.createQuery(sqlquery)
						.addParameter("idjenis", idjenis)
						.executeAndFetchTable().asList();
				
				JsonObject jObj		= new JsonObject();
				int intVal 			= (int) getReportData.get(0).get("idjenis");
				String strVal		= (String) getReportData.get(0).get("jenis");
				jObj.addProperty("idjenis", intVal);
				jObj.addProperty("jenis", strVal);
				
				jResult.addProperty("status", 200);
				jResult.add("data_Menu", jObj);
				
			}
			else {
				jResult.addProperty("status", 305);
			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postInsertKategori(Sql2o sql2o, String kategori) {

		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select * FROM master.ms_kategori WHERE nama_kategori_sparepart = :nama_kategori_sparepart";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("nama_kategori_sparepart", kategori)
					.executeAndFetchTable().asList();
			
			int jenisCount 	= getReportData.size();
			if(jenisCount == 0) {
				sqlquery 	= "INSERT INTO master.ms_kategori (nama_kategori_sparepart) VALUES (:nama_kategori_sparepart)";
				
				int idkategori = (int)conn.createQuery(sqlquery)
				.addParameter("nama_kategori_sparepart", kategori)
				.executeUpdate()
				.getKey();
				conn.commit();
				
				sqlquery 	= "select * FROM master.ms_kategori WHERE id = :id";
				getReportData = conn.createQuery(sqlquery)
						.addParameter("id", idkategori)
						.executeAndFetchTable().asList();
				
				JsonObject jObj		= new JsonObject();
				int intVal 			= (int) getReportData.get(0).get("id");
				String strVal		= (String) getReportData.get(0).get("nama_kategori_sparepart");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_kategori_sparepart", strVal);
				
				jResult.addProperty("status", 200);
				jResult.add("data_Kategori", jObj);
				
			}
			else {
				jResult.addProperty("status", 305);
			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postInsertMasterIT(Sql2o sql2o, Request req) {
		String strBody 		= req.body();
		JsonObject jBody	= new Gson().fromJson(strBody, JsonObject.class);
		int jenis_id 		= jBody.get("jenis_id").getAsInt();
		String jenis 		= jBody.get("jenis").getAsString();
		int jenis_detail_id = jBody.get("jenis_detail_id").getAsInt();
		String jenis_detail = jBody.get("jenis_detail").getAsString();
		String merk 		= jBody.get("merk").getAsString();
		String satuan 		= jBody.get("satuan").getAsString();
		String type 		= jBody.get("type").getAsString();
		String deskripsi 	= jBody.get("deskripsi").getAsString();
		int user_id 		= jBody.get("user_id").getAsInt();
		
		JsonObject jResult 	= new JsonObject();
		
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select tipe FROM master.ms_barang WHERE tipe = :tipe";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("tipe", type)
					.executeAndFetchTable().asList();
			
			int tipeCount 	= getReportData.size();
			if(tipeCount > 0) {
				jResult.addProperty("status", 400);
				jResult.addProperty("message", "Tipe Sama");
			}
			else {
				int numDigits 	= String.valueOf(user_id).length();
				if(numDigits == 0) {
					jResult.addProperty("status", 400);
					jResult.addProperty("message", "User 0");
				}
				else {
					sqlquery 	= "SELECT * FROM master.ms_jenis where jenis=:jenis";
					
					getReportData = conn.createQuery(sqlquery).addParameter("jenis", jenis)
							.executeAndFetchTable().asList();
					
					int idAwal 	= (int) getReportData.get(0).get("idjenis");
					
					sqlquery 	= "SELECT count (*) FROM master.ms_barang where jenis=:jenis";
					
					int countBarang = (int) conn.createQuery(sqlquery).addParameter("jenis", jenis)
							.executeScalar(Integer.class) + 1;
					
					// Format leading zero for 6 digits number
					String strCountBarang 	= String.format("%04d", countBarang);
					// Add kategori before code
					String kode_barang 		= idAwal + "/" + strCountBarang;
					
					String nama 			= (jenis_detail +" "+ merk + " " + type).toUpperCase();
					String nama_panjang 	= (jenis_detail +" "+ merk + " " + type).toUpperCase();
					
					int aktifStat 			= 1;
					
					sqlquery = "INSERT INTO master.ms_barang (kode_brg, nama, namapanjang, satuan, deskripsi, jenis_id, "
							+ "jenis, jenis_detail, jenis_detail_id, aktif, tgl_act, merk, tipe, user_id) VALUES (:kode_brg, "
							+ ":nama, :namapanjang, :satuan, :deskripsi, :jenis_id, :jenis, :jenis_detail, :jenis_detail_id, "
							+ ":aktif, :fulldate, :merk, :type, :user_id)";
					
					int idbarang = (int)conn.createQuery(sqlquery)
						.addParameter("kode_brg", kode_barang)
						.addParameter("nama", nama)
						.addParameter("namapanjang", nama_panjang)
						.addParameter("satuan", satuan)
						.addParameter("deskripsi", deskripsi)
						.addParameter("jenis_id", jenis_id)
						.addParameter("jenis", jenis)
						.addParameter("jenis_detail", jenis_detail)
						.addParameter("jenis_detail_id", jenis_detail_id)
						.addParameter("aktif", aktifStat)
						.addParameter("fulldate", new Date())
						.addParameter("merk", merk)
						.addParameter("type", type)
						.addParameter("user_id", user_id)						
						.executeUpdate()
						.getKey();
						conn.commit();
										
					jResult.addProperty("status", 200);
					jResult.addProperty("message", "sukses");
					jResult.addProperty("kode_brg", kode_barang);
				}
			}
			
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postInsertMasterSoftware(Sql2o sql2o, Request req) {

		String strBody 				= req.body();
		JsonObject jBody			= new Gson().fromJson(strBody, JsonObject.class);
		String software_nama		= jBody.get("software_nama").getAsString();
		int jenis_id				= jBody.get("jenis_id").getAsInt();
		String jenis_nama			= jBody.get("jenis_nama").getAsString();
		String software_spesifikasi	= jBody.get("software_spesifikasi").getAsString();
		String versi				= jBody.get("versi").getAsString();
		String linkurl				= jBody.get("linkurl").getAsString();
		
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select * FROM master.ms_software WHERE software_nama = :software_nama";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("software_nama", software_nama)
					.executeAndFetchTable().asList();
			
			int jenisCount 	= getReportData.size();
//			if(jenisCount == 0) {
				sqlquery 	= "INSERT INTO master.ms_software (software_nama, jenis_id, jenis_nama, software_spesifikasi, versi, aktif, linkurl) "
						+ "VALUES (:software_nama, :jenis_id, :jenis_nama, :software_spesifikasi, :versi, :aktif, :linkurl)";
				
				int idsoftware = (int)conn.createQuery(sqlquery)
				.addParameter("software_nama", software_nama)
				.addParameter("jenis_id", jenis_id)
				.addParameter("jenis_nama", jenis_nama)
				.addParameter("software_spesifikasi", software_spesifikasi)
				.addParameter("versi", versi)
				.addParameter("aktif", aktifStat)
				.addParameter("linkurl", linkurl)
				.executeUpdate()
				.getKey();
				conn.commit();
				
				sqlquery 	= "select * FROM master.ms_software WHERE software_id = :idsoftware";
				getReportData = conn.createQuery(sqlquery)
						.addParameter("idsoftware", idsoftware)
						.executeAndFetchTable().asList();
				
				JsonObject jObj		= new JsonObject();
				int intVal 			= (int) getReportData.get(0).get("software_id");
				String strVal		= (String) getReportData.get(0).get("software_nama");
				int intVal2			= (int) getReportData.get(0).get("jenis_id");
				String strVal1		= (String) getReportData.get(0).get("jenis_nama");
				String strVal2		= (String) getReportData.get(0).get("software_spesifikasi");
				String strVal3		= (String) getReportData.get(0).get("versi");
				String strVal4		= valueToStringOrEmptyString(getReportData.get(0), "linkurl");//(String) getReportData.get(0).getOrDefault("linkurl", "");
				
				jObj.addProperty("software_id", intVal);
				jObj.addProperty("software_nama", strVal);
				jObj.addProperty("jenis_id", intVal2);
				jObj.addProperty("jenis_nama", strVal1);
				jObj.addProperty("software_spesifikasi", strVal2);
				jObj.addProperty("versi", strVal3);
				jObj.addProperty("linkurl", strVal4);
				
				jResult.addProperty("status", 200);
				jResult.add("software", jObj);
				
//			}
//			else {
//				jResult.addProperty("status", 305);
//			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postInsertMasterSoftwareJenis(Sql2o sql2o, Request req) {

		String strBody 		= req.body();
		System.out.println(strBody);
		JsonObject jBody	= new Gson().fromJson(strBody, JsonObject.class);
		String jenis_nama	= jBody.get("jenis_nama").getAsString();
		System.out.print(jenis_nama);
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select * FROM master.ms_jenis_software WHERE jenis_nama = :jenis_nama";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("jenis_nama", jenis_nama)
					.executeAndFetchTable().asList();
			
			int jenisCount 	= getReportData.size();
			if(jenisCount == 0) {
				sqlquery 	= "INSERT INTO master.ms_jenis_software (jenis_nama, aktif) VALUES (:jenis_nama, :aktif)";
				
				int idjenis = (int)conn.createQuery(sqlquery)
					.addParameter("jenis_nama", jenis_nama)
					.addParameter("aktif", aktifStat)
					.executeUpdate()
					.getKey();
					conn.commit();
				
				sqlquery 	= "select * FROM master.ms_jenis_software WHERE jenis_id = :idjenis";
				getReportData = conn.createQuery(sqlquery)
						.addParameter("idjenis", idjenis)
						.executeAndFetchTable().asList();
				
				JsonObject jObj		= new JsonObject();
				int intVal 			= (int) getReportData.get(0).get("jenis_id");
				String strVal		= (String) getReportData.get(0).get("jenis_nama");
				jObj.addProperty("id", intVal);
				jObj.addProperty("nama_software", strVal);
				
				jResult.addProperty("status", 200);
				jResult.add("software_jenis", jObj);
				
			}
			else {
				jResult.addProperty("status", 305);
			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postInsertMasterToner(Sql2o sql2o, Request req) {
		
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.so_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_penerimaan_toner", jObj);
				
				return jResult.toString();
			}
			
			String noSO			= arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			String serialNumber	= arrStrResult[4];
			String tonerNama	= arrStrResult[5];
			System.out.println("strnoso= " + noSO);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_so", noSO);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			jObj.addProperty("sn", serialNumber);
			jObj.addProperty("toner_nama", tonerNama);
			
			jResult.addProperty("status", 200);
			jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String postInsertMasterTonerWOSP(Sql2o sql2o, Request req) {
		
		String strBody 				= req.body();
		JsonObject jBody			= new Gson().fromJson(strBody, JsonObject.class);
		int sparepart_id			= jBody.get("sparepart_id").getAsInt();
		String sparepart_nama		= jBody.get("sparepart_nama").getAsString();
		String sparepart_type		= jBody.get("sparepart_type").getAsString();
		int user_id					= jBody.get("user_id").getAsInt();
		String warna 				= jBody.get("warna").getAsString();
		String keterangan 			= jBody.get("keterangan").getAsString();
		int originalitas 			= jBody.get("is_ori").getAsInt();
		
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		String result_msg	="Sukses Insert";
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select count (id) FROM master.ms_toner WHERE sparepart_id = :sparepart_id";
			int spCount		= conn.createQuery(sqlquery).addParameter("sparepart_id", sparepart_id).executeScalar(Integer.class) + 1;
			
			String serial_number = sparepart_type + "-" +spCount;
			
			sqlquery	= "select * FROM master.ms_toner WHERE serial_number = :serial_number";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("serial_number", serial_number)
					.executeAndFetchTable().asList();
			
			int serialCount	= getReportData.size();
			
			JsonObject jObj			= new JsonObject();
			if(serialCount == 0) {
				String strNamaToner = sparepart_nama + " S/N " + serial_number;
				sqlquery 	= "INSERT INTO master.ms_toner (toner_nama, sparepart_id, sparepart_nama, aktif, user_id, "
						+ "tgl_insert, serial_number, warna, keterangan, originalitas) VALUES (:toner_nama, :sparepart_id, "
						+ ":sparepart_nama, :aktif, :user_id, :tgl_insert, :serial_number, :warna, :keterangan, :originalitas)";
				
				int idtoner = (int)conn.createQuery(sqlquery)
				.addParameter("toner_nama", strNamaToner)
				.addParameter("sparepart_id", sparepart_id)
				.addParameter("sparepart_nama", sparepart_nama)
				.addParameter("aktif", aktifStat)
				.addParameter("user_id", user_id)
				.addParameter("tgl_insert", new Date())
				.addParameter("serial_number", serial_number)
				.addParameter("warna", warna)
				.addParameter("keterangan", keterangan)
				.addParameter("originalitas", originalitas)
				.executeUpdate()
				.getKey();
				conn.commit();
				
				sqlquery 		= "select * FROM master.ms_toner WHERE id = :idtoner";
				getReportData 	= conn.createQuery(sqlquery)
						.addParameter("idtoner", idtoner)
						.executeAndFetchTable().asList();
				
				
				int toner_id 			= (int) getReportData.get(0).get("id");
				String toner_nama		= (String) getReportData.get(0).get("toner_nama");
				int part_id				= (int) getReportData.get(0).get("sparepart_id");
				String part_nama		= (String) getReportData.get(0).get("sparepart_nama");
				String serialnumber		= (String) getReportData.get(0).get("serial_number");
				String r_warna 			= (String) getReportData.get(0).get("warna");
				String r_keterangan 	= (String) getReportData.get(0).get("keterangan");
				int r_originalitas 		= (int) getReportData.get(0).get("originalitas");
				
				jObj.addProperty("toner_id", toner_id);
				jObj.addProperty("toner_nama", toner_nama);
				jObj.addProperty("sparepart_id", part_id);
				jObj.addProperty("sparepart_nama", part_nama);
				jObj.addProperty("serial_number", serialnumber);
				jObj.addProperty("warna", r_warna);
				jObj.addProperty("keterangan", r_keterangan);
				jObj.addProperty("originalitas", r_originalitas);
				jObj.addProperty("additional_message", result_msg);
				
				jResult.addProperty("status", 200);
				
			}
			else {
				result_msg = "duplicate entry";
				jObj.addProperty("additional_message", result_msg);
				jResult.addProperty("status", 403);
			}
			jResult.add("toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postInsertMasterStockOpname(Sql2o sql2o, Request req) {
		String strBody 		= req.body();
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		String result_msg	="Sukses Insert";
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery		= "select master.mapping_barang_detail_func2('"+ strBody +"'::json)";
//			System.out.println("query= " + sqlquery);
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
//			"(MPH/2021-02/0010,\"Laptope Agung\")"
			
			String [] arrStrResult = strSPResult.split("#may#");
			String strnomapping = arrStrResult[0];
			System.out.println("strnomapping= " + strnomapping);
			
			sqlquery		= "select * FROM master.ms_mapping_barang_ruangan WHERE no_mapping = :no_mapping";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("no_mapping", strnomapping)
					.executeAndFetchTable().asList();
			
			JsonObject jObj			= new JsonObject();
			
			String noMapping = (String) getReportData.get(0).get("no_mapping");
			String kodeBarang = valueToStringOrEmptyString(getReportData.get(0), "kode_barang");
			String namaBarang = valueToStringOrEmptyString(getReportData.get(0), "nama_barang");
			String serialNumber = valueToStringOrEmptyString(getReportData.get(0), "serial_number");
			String namaRuangan = valueToStringOrEmptyString(getReportData.get(0), "nama_ruangan");
			String kib = valueToStringOrEmptyString(getReportData.get(0), "kib");
			String pegawaiNama = valueToStringOrEmptyString(getReportData.get(0), "pegawai_nama");
			String pegawaiJabatan = valueToStringOrEmptyString(getReportData.get(0), "pegawai_jabatan");
			String ipLan = valueToStringOrEmptyString(getReportData.get(0), "ip_lan");
			String ipWifi = valueToStringOrEmptyString(getReportData.get(0), "ip_wifi") ;
			
			
			jObj.addProperty("noMapping", noMapping);
			jObj.addProperty("kodeBarang", kodeBarang);
			jObj.addProperty("namaBarang", namaBarang);
			jObj.addProperty("serialNumber", serialNumber);
			jObj.addProperty("namaRuangan", namaRuangan);
			jObj.addProperty("kib", kib);
			jObj.addProperty("pegawaiNama", pegawaiNama);
			jObj.addProperty("pegawaiJabatan", pegawaiJabatan);
			jObj.addProperty("ipLan", ipLan);
			jObj.addProperty("ipWifi", ipWifi);
			
			jResult.addProperty("status", 200);
//			jResult.addProperty("no_mapping", strnomapping);
			jResult.add("Data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postInsertMerk(Sql2o sql2o, String merk) {

		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		try(Connection conn	= sql2o.open()){
			
			String sqlquery	= "select * FROM master.ms_merk WHERE merk = :merk";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("merk", merk)
					.executeAndFetchTable().asList();
			
			int jenisCount 		= getReportData.size();
			if(jenisCount == 0) {
				sqlquery 		= "INSERT INTO master.ms_merk (merk, aktif) VALUES (:merk, :aktif)";
				
				int idmerk 		= (int)conn.createQuery(sqlquery)
				.addParameter("merk", merk)
				.addParameter("aktif", aktifStat)
				.executeUpdate()
				.getKey();
				conn.commit();
				
				sqlquery 		= "select * FROM master.ms_merk WHERE idmerk = :idmerk";
				getReportData 	= conn.createQuery(sqlquery)
						.addParameter("idmerk", idmerk)
						.executeAndFetchTable().asList();
				
				JsonObject jObj		= new JsonObject();
				int intVal 			= (int) getReportData.get(0).get("idmerk");
				String strVal		= (String) getReportData.get(0).get("merk");
				jObj.addProperty("idmerk", intVal);
				jObj.addProperty("merk", strVal);
				
				jResult.addProperty("status", 200);
				jResult.add("data_Merk", jObj);
				
			}
			else {
				jResult.addProperty("status", 305);
			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postInsertSparepart(Sql2o sql2o, Sql2o sql2oMysql, Request req) {

		String strBody 					= req.body();
		JsonObject jBody				= new Gson().fromJson(strBody, JsonObject.class);
		int sparepart_kategori 			= jBody.get("sparepart_kategori").getAsInt();
		String sparepart_kategori_nama 	= jBody.get("sparepart_kategori_nama").getAsString();
		int sparepart_merk 				= jBody.get("sparepart_merk").getAsInt();
		String sparepart_merk_nama 		= jBody.get("sparepart_merk_nama").getAsString();
		int sparepart_jenis 			= jBody.get("sparepart_jenis").getAsInt();
		String sparepart_jenis_nama 	= jBody.get("sparepart_jenis_nama").getAsString();
		int sparepart_port 				= jBody.get("sparepart_port").getAsInt();
		String sparepart_port_nama 		= jBody.get("sparepart_port_nama").getAsString();
		String sparepart_kapasistas 	= jBody.get("sparepart_kapasistas").getAsString();
		String sparepart_tipe 			= jBody.get("sparepart_tipe").getAsString();
		String sparepart_spesifikasi 	= jBody.get("sparepart_spesifikasi").getAsString();
		int user_id 					= jBody.get("user_id").getAsInt();
		
		JsonObject jResult 				= new JsonObject();
		int aktifStat 					= 1;
		
		try(Connection conn= sql2o.open(); Connection connMysql= sql2oMysql.open()){
			
			sparepart_tipe = sparepart_tipe.replaceAll("\\s+","");
			
			String sparepart_nama 	= sparepart_kategori_nama + " " + sparepart_merk_nama + " " + sparepart_kapasistas + " " + sparepart_tipe; 
			
			String sqlquery			= "select * FROM master.ms_sparepart WHERE sparepart_nama = :sparepart_nama";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery).addParameter("sparepart_nama", sparepart_nama)
					.executeAndFetchTable().asList();
			
			int jenisCount 			= getReportData.size();
			if(jenisCount == 0) {
				
				sqlquery 			= "SELECT count(sparepart_id) FROM master.ms_sparepart where sparepart_kategori=:sparepart_kategori";
				
				int countSp 		= (int) conn.createQuery(sqlquery).addParameter("sparepart_kategori", sparepart_kategori)
						.executeScalar(Integer.class) + 1;
				
				// Format leading zero for 6 digits number
				String codeSp 		= String.format("%06d", countSp);
				// Add kategori before code
				long sparepart_id 	= Long.parseLong(sparepart_kategori + codeSp);
				
				sqlquery = "INSERT INTO master.ms_sparepart (sparepart_id, sparepart_kategori, sparepart_merk, sparepart_jenis, "
						+ "sparepart_port, sparepart_kapasitas, sparepart_tipe, sparepart_spesifikasi, aktif, user_id, tgl_insert, "
						+ "sparepart_nama) VALUES (:sparepart_id, :sparepart_kategori, :sparepart_merk, :sparepart_jenis, "
						+ ":sparepart_port, :sparepart_kapasistas, :sparepart_tipe, :sparepart_spesifikasi, :aktif, :user_id, "
						+ ":fulldate, :sparepart_nama )";
				
				long idsparepart = (long)conn.createQuery(sqlquery)
				.addParameter("sparepart_id", sparepart_id)
				.addParameter("sparepart_kategori", sparepart_kategori)
				.addParameter("sparepart_merk", sparepart_merk)
				.addParameter("sparepart_jenis", sparepart_jenis)
				.addParameter("sparepart_port", sparepart_port)
				.addParameter("sparepart_kapasistas", sparepart_kapasistas)
				.addParameter("sparepart_tipe", sparepart_tipe)
				.addParameter("sparepart_spesifikasi", sparepart_spesifikasi)
				.addParameter("aktif", aktifStat)
				.addParameter("user_id", user_id)
				.addParameter("fulldate", new Date())
				.addParameter("sparepart_nama", sparepart_nama)
				.executeUpdate()
				.getKey();
				conn.commit();
				
				sqlquery 		= "select * FROM master.ms_sparepart WHERE sparepart_id = :sparepart_id";
				getReportData 	= conn.createQuery(sqlquery)
						.addParameter("sparepart_id", sparepart_id)
						.executeAndFetchTable().asList();
				
				JsonObject jObj						= new JsonObject();
				long valsparepart_id 				= (long) getReportData.get(0).get("sparepart_id");
				int valsparepart_kategori			= (int) getReportData.get(0).get("sparepart_kategori");
				String valsparepart_kategori_nama	= sparepart_kategori_nama;
				int valsparepart_merk				= (int) getReportData.get(0).get("sparepart_merk");
				String valsparepart_merk_nama		= sparepart_merk_nama;
				int valsparepart_jenis				= (int) getReportData.get(0).get("sparepart_jenis");
				String valsparepart_jenis_nama		= sparepart_jenis_nama;
				String valsparepart_port_nama		= sparepart_port_nama;
				String valsparepart_kapasistas		= (String) getReportData.get(0).get("sparepart_kapasitas");
				String valsparepart_tipe			= (String) getReportData.get(0).get("sparepart_tipe");
				String valsparepart_spesifikasi		= (String) getReportData.get(0).get("sparepart_spesifikasi");
				int valuserId						= (int) getReportData.get(0).get("user_id");
				
				sqlquery		 	= "select * from pegawai where pegawai_id = :id";
				getReportData 		= connMysql.createQuery(sqlquery).addParameter("id", valuserId)
						.executeAndFetchTable().asList();
				
				Map<String, Object> mapSql 	= getReportData.get(0);
				String username 			= (String) mapSql.get("nama");
				
				jObj.addProperty("sparepart_id", valsparepart_id);
				jObj.addProperty("sparepart_kategori", valsparepart_kategori);
				jObj.addProperty("sparepart_kategori_nama", valsparepart_kategori_nama);
				jObj.addProperty("sparepart_merk", valsparepart_merk);
				jObj.addProperty("sparepart_merk_nama", valsparepart_merk_nama);
				jObj.addProperty("sparepart_jenis", valsparepart_jenis);
				jObj.addProperty("sparepart_jenis_nama", valsparepart_jenis_nama);
				jObj.addProperty("sparepart_nama", sparepart_nama);
				jObj.addProperty("sparepart_port_nama", valsparepart_port_nama);
				jObj.addProperty("sparepart_kapasistas", valsparepart_kapasistas);
				jObj.addProperty("sparepart_tipe", valsparepart_tipe);
				jObj.addProperty("sparepart_spesifikasi", valsparepart_spesifikasi);
				jObj.addProperty("userinput", username);
				
				jResult.addProperty("status", 200);
				jResult.add("detail_insert", jObj);
				
			}
			else {
				jResult.addProperty("status", 305);
			}
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}

	private String postMenu(Sql2o sql2o, Request req) {
		
		String strBody 		= req.body();
		JsonObject jBody	= new Gson().fromJson(strBody, JsonObject.class);
		int pegawai_id		= jBody.get("pegawai_id").getAsInt();
//		int unit_id			= jBody.get("unit_id").getAsInt();
//		String nama			= jBody.get("nama").getAsString();
		
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		String nama_group	="";
		String resultMsg	="sukses";
		JsonArray jarr = new JsonArray();
		JsonArray jarrMenuAll = new JsonArray();
		JsonArray jarrButton = new JsonArray();
		
		try(Connection conn= sql2o.open()){
			
			JsonObject jMenuData = new JsonObject();
			
			String sqlquery= "select * from master.ms_user where pegawai_id =:pegawai_id and aktif=1";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("pegawai_id", pegawai_id) 
					.executeAndFetchTable().asList();
			
			int userCount = getReportData.size();
			
			if(userCount == 0) {
				resultMsg="Unauthorized User";
				jResult.addProperty("status", 401);
				return jResult.toString();
			}
			else {
				int groupid = (int) getReportData.get(0).get("group_id");
				sqlquery = "select nama_group from master.ms_user_group where id = :group_id and aktif=1";
				nama_group = (String)conn.createQuery(sqlquery)
						.addParameter("group_id", groupid)
						.executeScalar();
				
//				sqlquery = "select menu_id from master.ms_user_akses where group_id = :group_id";
//				List<Integer> listmenu = conn.createQuery(sqlquery)
//						.addParameter("group_id", groupid)
//						.executeScalarList(Integer.class);
//				
//				jResult.addProperty("nama_group", nama_group);
				
				sqlquery= "select m.* from master.ms_menu_y as m "
						+ "inner join master.ms_user_akses as a on a.menu_id=m.id "
						+ "inner join master.ms_user_group as g on g.id=a.group_id "
						+ "where parent_id=0 and a.group_id=:group_id order by urut";
				getReportData = conn.createQuery(sqlquery)
						.addParameter("group_id", groupid)
						.executeAndFetchTable().asList();
				
				List<JsonObject> jListSubMenu = new ArrayList<JsonObject>();
				List<JsonObject> jListMenu = new ArrayList<JsonObject>();
				
				for (Map<String, Object> map : getReportData) {
					JsonObject jObj = new JsonObject();
					
					int id					= (int) map.get("id");
					String menu_nama 		= (String) map.get("nama");
					String url 				= (String) map.get("url");
					int last 				= (int) map.get("last");
					String icon				= (String) map.get("icon");
					
					jObj.addProperty("menu_id", id);
					jObj.addProperty("menu_nama", menu_nama);
					jObj.addProperty("url", url);
					jObj.addProperty("icon", icon);
					
					if(last == 0) {
						JsonArray jarr2= new JsonArray();
						
						sqlquery= "select m.* from master.ms_menu_y as m "
								+ "inner join master.ms_user_akses as a on a.menu_id=m.id "
								+ "inner join master.ms_user_group as g on g.id=a.group_id "
								+ "where parent_id=:id and a.group_id=:group_id and m.level = 2 order by urut";
						List<Map<String,Object>> getReportData2 = conn.createQuery(sqlquery)
								.addParameter("id", id)
								.addParameter("group_id", groupid)
								.executeAndFetchTable().asList();
						for (Map<String, Object> map2 : getReportData2) {
							
							int id2					= (int) map2.get("id");
							String menu_nama2 		= (String) map2.get("nama");
							String url2 			= (String) map2.get("url");
							int last2 				= (int) map2.get("last");
							String icon2			= (String) map2.get("icon");
							
							JsonObject jObj2 = new JsonObject();
							
							jObj2.addProperty("menu_id", id2);
							jObj2.addProperty("menu_nama", menu_nama2);
							jObj2.addProperty("url", url2);
							jObj2.addProperty("icon", icon2);
							jarr2.add(jObj2);
						}
						jObj.add("subMenu", jarr2);
					}
					
					jarr.add(jObj);
				
				}
				
				sqlquery= "select m.* from master.ms_menu_y as m "
						+ "inner join master.ms_user_akses as a on a.menu_id=m.id "
						+ "inner join master.ms_user_group as g on g.id=a.group_id "
						+ "where a.group_id=:group_id and m.aktif_fb = 1 order by urut";
				getReportData = conn.createQuery(sqlquery)
						.addParameter("group_id", groupid)
						.executeAndFetchTable().asList();
				
				for (Map<String, Object> map : getReportData) {
					JsonObject jObj = new JsonObject();
					
					int id					= (int) map.get("id");
					String menu_nama 		= (String) map.get("nama");
					String url 				= (String) map.get("url");
					int last 				= (int) map.get("last");
					String icon				= (String) map.get("icon");
					
					jObj.addProperty("menu_id", id);
					jObj.addProperty("menu_nama", menu_nama);
					jObj.addProperty("url", url);
					jObj.addProperty("icon", icon);
					
					jarrButton.add(jObj);
				}
			}
			
			
			jResult.addProperty("status", 200);
			jResult.add("menu", jarr);
			jResult.add("button", jarrButton);
//			jResult.add("menu_all", jarrMenuAll);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
		
	private String postMenu2(Sql2o sql2o, Request req) {
		
		String strBody 		= req.body();
		JsonObject jBody	= new Gson().fromJson(strBody, JsonObject.class);
		int pegawai_id		= jBody.get("pegawai_id").getAsInt();
//		int unit_id			= jBody.get("unit_id").getAsInt();
//		String nama			= jBody.get("nama").getAsString();
		
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		String nama_group	="";
		String resultMsg	="sukses";
		JsonArray jarr = new JsonArray();
		JsonArray jarrMenuAll = new JsonArray();
		JsonArray jarrButton = new JsonArray();
		
		try(Connection conn= sql2o.open()){
			
			JsonObject jMenuData = new JsonObject();
			
			String sqlquery= "select * from master.ms_user where pegawai_id =:pegawai_id and aktif=1";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("pegawai_id", pegawai_id) 
					.executeAndFetchTable().asList();
			
			int userCount = getReportData.size();
			
			if(userCount == 0) {
				resultMsg="Unauthorized User";
				jResult.addProperty("status", 401);
				return jResult.toString();
			}
			else {
				int groupid = (int) getReportData.get(0).get("group_id");
				sqlquery = "select nama_group from master.ms_user_group where id = :group_id and aktif=1";
				nama_group = (String)conn.createQuery(sqlquery)
						.addParameter("group_id", groupid)
						.executeScalar();
				
				sqlquery = "select menu_id from master.ms_user_akses where group_id = :group_id";
				List<Integer> listmenu = conn.createQuery(sqlquery)
						.addParameter("group_id", groupid)
						.executeScalarList(Integer.class);
				
				jResult.addProperty("nama_group", nama_group);
				
				sqlquery= "select * from master.ms_menu_y";
				getReportData = conn.createQuery(sqlquery).executeAndFetchTable().asList();
				
				List<JsonObject> jListSubMenu = new ArrayList<JsonObject>();
				List<JsonObject> jListMenu = new ArrayList<JsonObject>();
				
				for (Map<String, Object> map : getReportData) {
					JsonObject jObj = new JsonObject();
					int menu_id				= (int) map.get("id");
					int parent_id 			= (int) map.get("parent_id");
					String menu_nama 		= (String) map.get("nama");
					String url 				= (String) map.get("url");
					int last 				= (int) map.get("last");
					String urut 			= (String) map.get("urut");
					String icon				= (String) map.get("icon");
					
					
					
					for (Integer idMenu : listmenu) {
						if(menu_id == idMenu) {
							jObj.addProperty("menu_id", menu_id);
							jObj.addProperty("parent_id", parent_id);
							jObj.addProperty("menu_nama", menu_nama);
							jObj.addProperty("url", url);
							jObj.addProperty("last", last);
							jObj.addProperty("urut", urut);
							jObj.addProperty("icon", icon);
							if(parent_id==0) {
								//jarr.add(jObj);
								jListMenu.add(jObj);
							}
							else
								jListSubMenu.add(jObj);
							break;
						}
					}
				}
				
				for (JsonObject menuHeader : jListMenu) {
					JsonArray jarrSubMenu = new JsonArray();
					for (JsonObject jObject : jListSubMenu) {
						if((jObject.get("parent_id").getAsInt() == menuHeader.get("menu_id").getAsInt())) {
							jarrSubMenu.add(jObject);
							jarrButton.add(jObject);
							jarrMenuAll.add(jObject);
						}
					}
					// Menampilkan sub menu dari sidebar
					menuHeader.add("subMenu", jarrSubMenu);
					jarr.add(menuHeader);
					jarrMenuAll.add(menuHeader);
				}
				
			}
			
			
			jResult.addProperty("status", 200);
			jResult.add("menu", jarr);
			jResult.add("button", jarrButton);
			jResult.add("menu_all", jarrMenuAll);
//			System.out.println("JsonArray= " + jarr.toString());
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postTransaksiLogCekToken(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("id_user").getAsInt();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		
		
		JsonObject jObj 	= new JsonObject();
		jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
		
		JsonObject jResult		= new JsonObject();
		jResult.addProperty("status", jCekToken.get("status").getAsInt());
		jResult.add("cek_token", jObj);
		
		return jResult.toString();
		
		/*
		JsonObject jResult		= new JsonObject();
		String result_msg;
		int httpcode			=200;
		
		try(Connection conn	= sql2o.open()){
			
//			String sqlquery	= "select * FROM master.ms_login_status "
//					+ "WHERE id_user = :id_user and token = :token and expired >= :currDate order by id desc";
			String sqlquery	= "select * FROM master.ms_login_status "
					+ "WHERE id_user = :id_user and token = :token order by id desc";

			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("id_user", id_user)
					.addParameter("token", token)
//					.addParameter("currDate", new Date())
					.executeAndFetchTable().asList();
			
			int idCount 		= getReportData.size();
			JsonObject jObj 	= new JsonObject();
			// Jika token dan id_user cocok 
			if(idCount > 0) {
				
				Date r_expired		= (Date)getReportData.get(0).get("expired");
				int r_is_active		= (int)getReportData.get(0).get("is_active");
				//Cek apakah token aktif
				if(r_is_active == 1) {
					// Cek apakah token expired
					if(r_expired.before(new Date())) {
						httpcode		= 401;
						result_msg = "Token anda telah expired";
					}
					else {
						result_msg = "Token aktif dan valid";
					}
				}
				else {
					// Token tidak aktif
					httpcode		= 401;
					result_msg = "Anda telah logout karena login dari device lain";
				}
				
			}
			else {
				result_msg 		= "Token tidak sesuai";
				httpcode		= 401;
			}
			jObj.addProperty("additional_message", result_msg);
			jResult.addProperty("status", httpcode);
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		*/
		
	}
	
	private String postTransaksiLogToken(Sql2o sql2o, Request req) {
		
		String strBody 				= req.body();
		JsonObject jBody			= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user					= jBody.get("id_user").getAsInt();
		int id_unit					= jBody.get("id_unit").getAsInt();
		String ip_address			= jBody.get("ip_address").getAsString();
		String mac_address			= jBody.get("mac_address").getAsString();
		String logstatus			= jBody.get("status").getAsString();
		
		String token;
		
		JsonObject jResult 	= new JsonObject();
		int aktifStat 		= 1;
		
		String resultMsg="";
		switch (logstatus) {
			case "login":
				try(Connection conn	= sql2o.open()){
					
					String sqlquery	= "select * FROM master.ms_login_status WHERE id_user = :id_user order by id desc";
					List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
							.addParameter("id_user", id_user)
							.executeAndFetchTable().asList();
										
					int idCount 	= getReportData.size();
					// Jika sudah ada data login
					if(idCount > 0) {
						int is_active	= (int)getReportData.get(0).get("is_active");
						int id 			= (int)getReportData.get(0).get("id");
						
						// Jika ada login yg masih aktif (is_active = 1), rubah ke non aktif (is_active = 0)
						if(is_active == 1) {
						
							sqlquery 		= "update master.ms_login_status set is_active = 0 where id = :id";
							
							int idUpdated 	= (int)conn.createQuery(sqlquery)
								.addParameter("id", id)
								.executeUpdate()
								.getKey();
							conn.commit();
							resultMsg 		= "Login anda pada device lain di non-aktifkan";
						}
						
					}
					
					// Buat Token Baru
					Date tokenStartDate = new Date();
					Timestamp ts		= new Timestamp(tokenStartDate.getTime());
					
					String rawToken 	= id_user + "-" + ts;
					token= BCrypt.hashpw(rawToken, BCrypt.gensalt(4));
					
					Calendar cal= Calendar.getInstance();
					cal.add(Calendar.DATE, 7);
					
					Date tokenExpiredDate = cal.getTime();
					
					// Insert token baru
					sqlquery 	= "INSERT INTO master.ms_login_status (id_user, token, expired, is_active, ip_address, mac_address, created_at, status, idunit) "
							+ "VALUES (:id_user, :token, :expired, :is_active, :ip_address, :mac_address, :created_at, :status, :id_unit)";
					
					int idlogin = (int)conn.createQuery(sqlquery, true)
						.addParameter("id_user", id_user)
						.addParameter("token", token)
						.addParameter("expired", tokenExpiredDate)
						.addParameter("is_active", aktifStat)
						.addParameter("ip_address", ip_address)
						.addParameter("mac_address", mac_address)
						.addParameter("created_at", tokenStartDate)
						.addParameter("status", logstatus)
						.addParameter("id_unit", id_unit)
						.executeUpdate()
						.getKey();
					conn.commit();
					
					sqlquery 	= "select ls.token, ls.expired, u.group_id from master.ms_login_status ls "
							+ "inner join master.ms_user u on ls.id_user = u.pegawai_id "
							+ "WHERE ls.id = :idlogin";
					getReportData = conn.createQuery(sqlquery)
							.addParameter("idlogin", idlogin)
							.executeAndFetchTable().asList();
					
					JsonObject jObj		= new JsonObject();
					String strToken		= (String) getReportData.get(0).get("token");
					int groupId			= (int) getReportData.get(0).get("group_id");
					Date dateVal		= (Date) getReportData.get(0).get("expired");
					String strDateVal	= dateTimeFormat.format(dateVal);
					if(resultMsg.length() == 0)
						resultMsg = "Mendapat token";
					
					jObj.addProperty("token_nice", strToken);
					jObj.addProperty("group_identifikasi", groupId);
					jObj.addProperty("expired", strDateVal);
					jObj.addProperty("additional_message", resultMsg);
					
					
					jResult.addProperty("status", 200);
					jResult.add("login_token", jObj);
					
					
					return jResult.toString();
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					jResult.addProperty("status", 400);
					
					return jResult.toString();
				}
	//			break;
			case "logout":
				token = jBody.get("token").getAsString();
				try(Connection conn	= sql2o.open()){
					
					String sqlquery	= "select * FROM master.ms_login_status WHERE id_user = :id_user and token = :token order by id desc";
					List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
							.addParameter("id_user", id_user)
							.addParameter("token", token)
							.executeAndFetchTable().asList();
					
					int idCount 		= getReportData.size();
					JsonObject jObj 	= new JsonObject();
					// Jika token dan id_user cocok
					if(idCount > 0) {
						
						int r_id_user 		= (int)getReportData.get(0).get("id_user");
						String r_strtoken 	= (String)getReportData.get(0).get("token");
						Date r_expired		= (Date)getReportData.get(0).get("expired");
						int r_is_active		= (int)getReportData.get(0).get("is_active");
						String r_ip_address	= (String)getReportData.get(0).get("ip_address");
						String r_mac_address= (String)getReportData.get(0).get("mac_address");
						Date r_created_at	= (Date)getReportData.get(0).get("expired");
						String r_status		= (String)getReportData.get(0).get("status");
						int r_id_unit 		= (int)getReportData.get(0).get("idunit");
						
						// Jika ada login yg aktif (is_active = 1), insert data logout is_active = 0
						if(r_is_active == 1) {
						
							sqlquery 		= "INSERT INTO master.ms_login_status (id_user, token, expired, is_active, ip_address, "
									+ "mac_address, created_at, status, idunit) VALUES (:id_user, :token, :expired, :is_active, "
									+ ":ip_address, :mac_address, :created_at, :status, :idunit)";
							
							int idLogout 	= (int)conn.createQuery(sqlquery)
								.addParameter("id_user", r_id_user)
								.addParameter("token", r_strtoken)
								.addParameter("expired", r_expired)
								.addParameter("is_active", 0)
								.addParameter("ip_address", r_ip_address)
								.addParameter("mac_address", r_mac_address)
								.addParameter("created_at", r_created_at)
								.addParameter("status", logstatus)
								.addParameter("idunit", r_id_unit)
								.executeUpdate()
								.getKey();
							conn.commit();
							resultMsg 		= "Logout berhasil";
						}
						else {
							resultMsg 		= "Anda telah logout sebelumnya";
						}
						
					}
					else {
						resultMsg 		= "Token tidak ditemukan, logout gagal";
					}
					jObj.addProperty("additional_message", resultMsg);
					jResult.addProperty("status", 200);
					jResult.add("logout_token", jObj);
					
					
					return jResult.toString();
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					jResult.addProperty("status", 400);
					return jResult.toString();
				}
	//			break;
	
			default:
				jResult.addProperty("status", 400);
				return jResult.toString();
	//			break;
		}
		
		
	}
	
	private String postTransaksiTonerPemakaian(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
		
		System.out.println("user_login: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.pemakaian_pengembalian_ubahstatus_pengeluaran_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah dipakai
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_pemakaian_toner", jObj);
				
				return jResult.toString();
			}
			
			String noPemakaian 		= arrStrResult[1];
			String noPengembalian	="";
			String tglAct			="";
			String userId			="";
			if(arrStrResult.length < 5) {
				tglAct		= arrStrResult[2];
				userId		= arrStrResult[3];
			}
			else {
				noPengembalian	= arrStrResult[2];
				tglAct			= arrStrResult[3];
				userId			= arrStrResult[4];
			}
			System.out.println("strnomapping= " + noPemakaian);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_pemakaian", noPemakaian);
			jObj.addProperty("no_pengembalian", noPengembalian);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data_pemakaian_toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postTransaksiTonerPengembalian(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_login").getAsInt();
		
		System.out.println("user_login: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.pemakaian_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah dipakai
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_pemakaian_toner", jObj);
				
				return jResult.toString();
			}
			
			String noPemakaian = arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			System.out.println("strnomapping= " + noPemakaian);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_penerimaan", noPemakaian);
//			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data_pemakaian_toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postTransaksiTonerPengembalianVerifikasi(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
		
		System.out.println("user_login: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.pengembalian_verif_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah dipakai
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data", jObj);
				
				return jResult.toString();
			}
			
			String tglAct		= arrStrResult[1];
			String userId		= arrStrResult[2];
//			System.out.println("strnomapping= " + noPemakaian);
			
			
			JsonObject jObj = new JsonObject();
			
			
//			jObj.addProperty("no_penerimaan", noPemakaian);
//			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	
	private String postTransaksiTonerPengeluaranBarang(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
		
		System.out.println("user_id: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.pengeluaran_barang_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah dipakai
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_pengeluaran", jObj);
				
				return jResult.toString();
			}
			
			String noPengeluaran = arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			System.out.println("strnomapping= " + noPengeluaran);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_pengeluaran", noPengeluaran);
//			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data_pengeluaran", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postTransaksiTonerPengeluaranBarangUbahStatus(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
//		int status				= jBody.get("status").getAsInt();
//		String noPengeluaran	= jBody.get("no_pengeluaran").getAsString();
//		String tonerId			= jBody.get("toner_id").getAsString();
		
		System.out.println("user_id: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.ubahstatus_pengeluaran_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_penerimaan_toner", jObj);
				
				return jResult.toString();
			}
			
			String tglAct		= arrStrResult[1];
			String userId		= arrStrResult[2];
			
			JsonObject jObj = new JsonObject();
			
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			//jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	
	private String postTransaksiTonerPOWOSO(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("id_user").getAsInt();
		int id_pihak3			= jBody.get("id_pihakketiga").getAsInt();
		String pihak3			= jBody.get("pihakketiga").getAsString();
		JsonArray jTonerArr		= jBody.get("arr_toner").getAsJsonArray();
		
		
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			//Initialize your Date however you like it.
			Date dateNow = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateNow);
			int year = calendar.get(Calendar.YEAR);
			//Add one to month {0 - 11}
			int month = calendar.get(Calendar.MONTH) + 1;
			
			String sqlquery	= "select * FROM transaksi.toner_po_header WHERE EXTRACT(YEAR FROM tgl_act) = :year "
					+ "AND EXTRACT(MONTH FROM tgl_act) = :month";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("year", year)
					.addParameter("month", month)
					.executeAndFetchTable().asList();
			
			int poIndex 	= getReportData.size() + 1;
			
			String noPO		= "POTR/"+year+"-"+month+"/"+poIndex;
			
			sqlquery 		= "INSERT INTO transaksi.toner_po_header (no_po, id_pihak_ketiga, nama_pihak_ketiga, tgl_act, user_id, status_po) "
					+ "VALUES (:no_po, :id_pihak_ketiga, :nama_pihak_ketiga, :tgl_act, :user_id, :status_po)";
			
			int idpo 		= (int)conn.createQuery(sqlquery)
			.addParameter("no_po", noPO)
			.addParameter("id_pihak_ketiga", id_pihak3)
			.addParameter("nama_pihak_ketiga", pihak3)
			.addParameter("tgl_act", dateNow)
			.addParameter("user_id", id_user)
			.addParameter("status_po", 1)
			.executeUpdate()
			.getKey();
//			conn.commit();
			
			sqlquery 		= "INSERT INTO transaksi.toner_po_detail (no_po, toner_id, toner_nama, jumlah, tgl_act, user_id, status) "
					+ "VALUES (:no_po, :toner_id, :toner_nama, :jumlah, :tgl_act, :user_id, :status)";
			
			Query query 	= conn.createQuery(sqlquery);
			
			for (JsonElement jElment : jTonerArr) {
				String tonerId 		= jElment.getAsJsonObject().get("toner_id").getAsString();
				String tonerNama 	= jElment.getAsJsonObject().get("toner_nama").getAsString();
				
				query.addParameter("no_po", noPO)
				.addParameter("toner_id", tonerId)
				.addParameter("toner_nama", tonerNama)
				.addParameter("jumlah", 1)
				.addParameter("tgl_act", dateNow)
				.addParameter("user_id", id_user)
				.addParameter("status", 1)
				.addToBatch();
			}
			
			query.executeBatch(); // executes entire batch
	        conn.commit();        // remember to call commit(), else sql2o will automatically rollback.
			
			sqlquery 		= "select * FROM transaksi.toner_po_header WHERE id = :idpo";
			getReportData 	= conn.createQuery(sqlquery)
					.addParameter("idpo", idpo)
					.executeAndFetchTable().asList();
			
			JsonObject jObj		= new JsonObject();
			int id				= (int) getReportData.get(0).get("id");
			String intVal 		= (String) getReportData.get(0).get("no_po");
			Date dateVal		= (Date) getReportData.get(0).get("tgl_act");
			jObj.addProperty("no_po", intVal);
			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", dateTimeFormat.format(dateVal));
			
			jResult.addProperty("status", 200);
			jResult.add("data_po_toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String postTransaksiTonerPO(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
//		int status				= jBody.get("status").getAsInt();
//		String noPengeluaran	= jBody.get("no_pengeluaran").getAsString();
//		String tonerId			= jBody.get("toner_id").getAsString();
		
		System.out.println("user_id: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.po_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data", jObj);
				
				return jResult.toString();
			}
			
			String noPO			= arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			
			JsonObject jObj = new JsonObject();
			
			jObj.addProperty("no_po", noPO);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	
	private String postTransaksiTonerPOnRetur(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
//		int status				= jBody.get("status").getAsInt();
//		String noPengeluaran	= jBody.get("no_pengeluaran").getAsString();
//		String tonerId			= jBody.get("toner_id").getAsString();
		
		System.out.println("user_id: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.po_retur_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_penerimaan_toner", jObj);
				
				return jResult.toString();
			}
			
			String noPO			= arrStrResult[1];
			String noRetur		= arrStrResult[2];
			String tglAct		= arrStrResult[3];
			String userId		= arrStrResult[4];
			
			JsonObject jObj = new JsonObject();
			
			jObj.addProperty("no_po", noPO);
			jObj.addProperty("no_retur", noRetur);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	
	private String postTransaksiTonerPOBatal(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("id_user").getAsInt();
		String noPO				= jBody.get("no_po").getAsString();
		int ketBatal			= jBody.get("ket_batal").getAsInt();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.po_batal_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, po toner sudah dibatalkan
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data", jObj);
				
				return jResult.toString();
			}
			
			String noPoBatal 	= arrStrResult[1];
			String noPo		 	= arrStrResult[2];
			String tglAct		= arrStrResult[3];
			String userId		= arrStrResult[4];
			System.out.println("strnomapping= " + noPoBatal);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_po_batal", noPoBatal);
			jObj.addProperty("no_po", noPo);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String postTransaksiTonerPOPenerimaan(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("id_user").getAsInt();
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.penerimaan_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_penerimaan_toner", jObj);
				
				return jResult.toString();
			}
			
			String noPenerimaan = arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			System.out.println("strnomapping= " + noPenerimaan);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_penerimaan", noPenerimaan);
//			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data_penerimaan_toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}

	private String postTransaksiTonerPOPenerimaanNoToken(Sql2o sql2o, Request req) {
		
//		String [] headerToken 	= req.headers("Authorization").split(" ");
//		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("id_user").getAsInt();
		
//		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
//		int stat				= jCekToken.get("status").getAsInt();
//		if(stat!=200) {
//			JsonObject jObj 	= new JsonObject();
//			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
//			
//			JsonObject jResult		= new JsonObject();
//			jResult.addProperty("status", jCekToken.get("status").getAsInt());
//			jResult.add("cek_token", jObj);
//			
//			return jResult.toString();
//		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.penerimaan_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data_penerimaan_toner", jObj);
				
				return jResult.toString();
			}
			
			String noPenerimaan = arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			System.out.println("strnomapping= " + noPenerimaan);
			
			
			JsonObject jObj = new JsonObject();
			
			
			jObj.addProperty("no_penerimaan", noPenerimaan);
//			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data_penerimaan_toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String postTransaksiTonerPOPenerimaanWOSP(Sql2o sql2o, Request req) {
		
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		String noPO				= jBody.get("no_po").getAsString();
		int id_user				= jBody.get("id_user").getAsInt();
		int id_pihak3			= jBody.get("id_pihakketiga").getAsInt();
		String pihak3			= jBody.get("pihakketiga").getAsString();
		int jumlahTotal			= jBody.get("jumlah_total").getAsInt();
		JsonArray jTonerArr		= jBody.get("arr_toner").getAsJsonArray();
		int statusPO			= 0;
		
		if(jTonerArr.size() < jumlahTotal) {
			statusPO			= 4;
		}
		else
			statusPO			= 3;
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			//Initialize your Date however you like it.
			Date dateNow = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateNow);
			int year = calendar.get(Calendar.YEAR);
			//Add one to month {0 - 11}
			int month = calendar.get(Calendar.MONTH) + 1;
			
			String sqlquery	= "select * FROM transaksi.toner_penerimaan_header WHERE EXTRACT(YEAR FROM tgl_act) = :year "
					+ "AND EXTRACT(MONTH FROM tgl_act) = :month";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("year", year)
					.addParameter("month", month)
					.executeAndFetchTable().asList();
			
			int poIndex 	= getReportData.size() + 1;
			
			String noTerima		= "TRMTR/"+year+"-"+month+"/"+poIndex;
			
			sqlquery 		= "INSERT INTO transaksi.toner_penerimaan_header (no_po, no_penerimaan, id_pihak_ketiga, nama_pihak_ketiga, tgl_act, user_id, jenis_penerimaan, status_penerimaan) "
					+ "VALUES (:no_po, :no_penerimaan, :id_pihak_ketiga, :nama_pihak_ketiga, :tgl_act, :user_id, :jenis_penerimaan, :status_penerimaan)";
			
			int idpenerimaan	= (int)conn.createQuery(sqlquery)
				.addParameter("no_po", noPO)
				.addParameter("no_penerimaan", noTerima)
				.addParameter("id_pihak_ketiga", id_pihak3)
				.addParameter("nama_pihak_ketiga", pihak3)
				.addParameter("tgl_act", dateNow)
				.addParameter("user_id", id_user)
				.addParameter("jenis_penerimaan", 1) // 1= penerimaan langsung; 2 = penerimaan tidak langsung
				.addParameter("status_penerimaan", 1) // 1= diterima, 2= batal
				.executeUpdate()
				.getKey();
//			conn.commit();
			
			sqlquery 		= "INSERT INTO transaksi.toner_penerimaan_detail (no_penerimaan, toner_id, toner_nama, jumlah, tgl_act, user_id, status) "
					+ "VALUES (:no_po, :toner_id, :toner_nama, :jumlah, :tgl_act, :user_id, :status)";
			
			Query query 	= conn.createQuery(sqlquery);
			
			for (JsonElement jElment : jTonerArr) {
				String tonerId 		= jElment.getAsJsonObject().get("toner_id").getAsString();
				String tonerNama 	= jElment.getAsJsonObject().get("toner_nama").getAsString();
				
				query.addParameter("no_po", noPO)
				.addParameter("toner_id", tonerId)
				.addParameter("toner_nama", tonerNama)
				.addParameter("jumlah", 1)
				.addParameter("tgl_act", dateNow)
				.addParameter("user_id", id_user)
				.addParameter("status", 1)
				.addToBatch();
			}
			
			query.executeBatch(); // executes entire batch
			
			sqlquery 		= "UPDATE transaksi.toner_po_header SET status_po = :statusPO WHERE no_po = :no_po";
			int idpo		= (int)conn.createQuery(sqlquery)
					.addParameter("statusPO", statusPO)
					.addParameter("no_po", noPO)
					.executeUpdate()
					.getKey();
			
			sqlquery 		= "UPDATE transaksi.toner_po_detail SET status = :status WHERE no_po = :no_po AND toner_id= :tonerId";
			
			query 			= conn.createQuery(sqlquery);
			
			for (JsonElement jElment : jTonerArr) {
				String tonerId 		= jElment.getAsJsonObject().get("toner_id").getAsString();
				
				query.addParameter("no_po", noPO)
				.addParameter("tonerId", tonerId)
				.addParameter("status", 3) // 3: diterima
				.addToBatch();
			}
			
			query.executeBatch(); // executes entire batch
		
	        conn.commit();        // remember to call commit(), else sql2o will automatically rollback.
			
			sqlquery 		= "select * FROM transaksi.toner_penerimaan_header WHERE id = :idpo";
			getReportData 	= conn.createQuery(sqlquery)
					.addParameter("idpo", idpenerimaan)
					.executeAndFetchTable().asList();
			
			JsonObject jObj		= new JsonObject();
			int id				= (int) getReportData.get(0).get("id");
			String intVal 		= (String) getReportData.get(0).get("no_penerimaan");
			Date dateVal		= (Date) getReportData.get(0).get("tgl_act");
			int userID			= (int) getReportData.get(0).get("user_id");
			
			jObj.addProperty("no_penerimaan", intVal);
			jObj.addProperty("id", id);
			jObj.addProperty("tgl_act", dateTimeFormat.format(dateVal));
			jObj.addProperty("user_id", userID);
			
			jResult.addProperty("status", 200);
			jResult.add("data_penerimaan_toner", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
		
	}
	
	private String postTransaksiTonerPODetail(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("id_user").getAsInt();
		String noPO				= jBody.get("no_po").getAsString();
		
		
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery	= "select * FROM transaksi.toner_po_detail WHERE no_po = :noPO";
			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("no_po", noPO)
					.executeAndFetchTable().asList();
			
			String tonerId 		= (String)getReportData.get(0).get("toner_id");
			String tonerNama 	= (String)getReportData.get(0).get("toner_nama");
			int status 			= (int)getReportData.get(0).get("status");
			Date tgl_act		= (Date)getReportData.get(0).get("tgl_act");
			
			JsonObject jObj		= new JsonObject();
			jObj.addProperty("toner_id", tonerId);
			jObj.addProperty("toner_nama", tonerNama);
			jObj.addProperty("status", status);
			jObj.addProperty("tgl_po", dateTimeFormat.format(tgl_act));
			
			jResult.addProperty("status", 200);
			jResult.add("data_po_toner_detail", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	private String postTransaksiTonerRetur(Sql2o sql2o, Request req) {
		String [] headerToken 	= req.headers("Authorization").split(" ");
		String token			= headerToken[1];
		
		String strBody 			= req.body();
		JsonObject jBody		= new Gson().fromJson(strBody, JsonObject.class);
		
		int id_user				= jBody.get("user_id").getAsInt();
//		int status				= jBody.get("status").getAsInt();
//		String noPengeluaran	= jBody.get("no_pengeluaran").getAsString();
//		String tonerId			= jBody.get("toner_id").getAsString();
		
		System.out.println("user_id: "+id_user);
		System.out.println("token: "+token);
		
		JsonObject jCekToken	= cekToken(sql2o, id_user, token);
		int stat				= jCekToken.get("status").getAsInt();
		if(stat!=200) {
			JsonObject jObj 	= new JsonObject();
			jObj.addProperty("additional_message", jCekToken.get("result_msg").getAsString());
			
			JsonObject jResult		= new JsonObject();
			jResult.addProperty("status", jCekToken.get("status").getAsInt());
			jResult.add("cek_token", jObj);
			
			return jResult.toString();
		}
		
		JsonObject jResult 	= new JsonObject();
		
		try(Connection conn= sql2o.open()){
			
			String sqlquery		= "select transaksi.retur_toner_func('"+ strBody +"'::json)";
			String strSPResult	= conn.createQuery(sqlquery).executeScalar(String.class);
			conn.commit();
			
			String [] arrStrResult = strSPResult.split("#may#");
			int resultStat		= Integer.parseInt(arrStrResult[0]);
			
			// Jika 400 maka, toner sudah diterima
			if( resultStat == 409 ) {
				JsonObject jObj 	= new JsonObject();
				jObj.addProperty("additional_message", arrStrResult[1]);
				
				jResult		= new JsonObject();
				jResult.addProperty("status", 409);
				jResult.add("data", jObj);
				
				return jResult.toString();
			}
			
			String noRetur		= arrStrResult[1];
			String tglAct		= arrStrResult[2];
			String userId		= arrStrResult[3];
			
			JsonObject jObj = new JsonObject();
			
			jObj.addProperty("no_retur", noRetur);
			jObj.addProperty("tgl_act", tglAct);
			jObj.addProperty("user_id", userId);
			
			jResult.addProperty("status", 200);
			jResult.add("data", jObj);
			return jResult.toString();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			return jResult.toString();
		}
	}
	
	
	private String jenisTransaksi(int codeTrans) {
		String result="";
		switch (codeTrans) {
			case 1:
				result= "Penerimaan";
				break;
			case 2:
				result= "Pemakaian";		
				break;
			case 5:
				result= "Retur Unit";
				break;
			case 6:
				result= "Retur Pihak ke tiga";
				break;
			case 8:
				result= "Purchase Order";
				break;
			case 9:
				result= "Batal Purchase Order";
				break;
			case 10:
				result= "Stock Opname";
				break;
	
			default:
				break;
		}
		return result;
	}
	
//	private String postTransksiTonerReturPenerimaan(Sql2o sql2o, Request req) {
//		
//	}
	
	// Integer data type is nullable
	private Integer valueToIntOrNull(Map<String, Object> mapData, String key) {
		Object valueResult = mapData.get(key);
		return valueResult == null ? null : (Integer) valueResult;
	}
	// Set Zero value for null result
	
	
	private int valueToIntOrZero(Map<String, Object> mapData, String key) {
		Object valueResult = mapData.get(key);
		return valueResult == null ? 0 : (int) valueResult;
	}
	
	// Avoiding null value 
	private String valueToStringOrEmptyString(Map<String, Object> mapData, String key) {
		Object valueResult = mapData.get(key);
		return valueResult == null ? "" : (String) valueResult;
	}
	
	private JsonObject cekToken(Sql2o sql2o, int id_user, String token) {
		JsonObject jResult		= new JsonObject();
		String result_msg;
		int httpcode			=200;
		
		try(Connection conn	= sql2o.open()){
			
//			String sqlquery	= "select * FROM master.ms_login_status "
//					+ "WHERE id_user = :id_user and token = :token and expired >= :currDate order by id desc";
			String sqlquery	= "select * FROM master.ms_login_status "
					+ "WHERE id_user = :id_user and token = :token order by id desc";

			List<Map<String,Object>> getReportData = conn.createQuery(sqlquery)
					.addParameter("id_user", id_user)
					.addParameter("token", token)
//					.addParameter("currDate", new Date())
					.executeAndFetchTable().asList();
			
			int idCount 		= getReportData.size();
			
			// Jika token dan id_user cocok 
			if(idCount > 0) {
				
				Date r_expired		= (Date)getReportData.get(0).get("expired");
				int r_is_active		= (int)getReportData.get(0).get("is_active");
				//Cek apakah token aktif
				if(r_is_active == 1) {
					// Cek apakah token expired
					if(r_expired.before(new Date())) {
						httpcode		= 401;
						result_msg = "Token anda telah expired";
					}
					else {
						result_msg = "Token aktif dan valid";
					}
				}
				else {
					// Token tidak aktif
					httpcode		= 401;
					result_msg = "Anda telah logout karena login dari device lain";
				}
				
			}
			else {
				result_msg 		= "Token tidak sesuai";
				httpcode		= 401;
			}
			
			jResult.addProperty("status", httpcode);
			jResult.addProperty("result_msg", result_msg);
			
			return jResult;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			jResult.addProperty("status", 400);
			jResult.addProperty("result_msg", "Bad Request");
			return jResult;
		}
	}
	
	public final static void apply() {
		Filter filter = new Filter() {

			@Override
			public void handle(Request request, Response response) throws Exception {
				// TODO Auto-generated method stub
				//request.type("application/json");
				System.out.println((new Date()).toString());
				System.out.println( request.headers().toString());
				Set<String> myHeaders= request.headers();
				for (String myheader : myHeaders) {
					System.out.println( "key : " + myheader);
					System.out.println( request.headers(myheader));
				}
				System.out.println("ip client : " + request.ip());
				response.header("Access-Control-Allow-Origin", "*");
				response.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
			}
			
		};
		//Spark.after(filter);
		Spark.before(filter);
	}
}
