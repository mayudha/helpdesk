package helpdesk;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class App {
	public static Properties prop;
		
	public static void main(String...args) {
		prop = new Properties();
		if(args.length > 0) {
			loadProperties(prop, args[0]);
		}
		else {
			System.out.println("Parameter Properties Empty");
			System.exit(0);
		}
		
		// Config Server
		int port = Integer.parseInt(prop.getProperty("server.port", "9891"));
		int maxThread = Integer.parseInt(prop.getProperty("server.maxthread", "10"));
		int minThread = Integer.parseInt(prop.getProperty("server.minthread", "7"));
		int timeOutMilis = Integer.parseInt(prop.getProperty("server.timeout", "1000"));
		
		// Config db
		String dbHost = prop.getProperty("db.host", "127.0.0.1");
		String dbPort = prop.getProperty("db.port", "5432");
		String dbName = prop.getProperty("db.name", "helpdesk");
		String dbUser = prop.getProperty("db.user", "postgres");
		String dbPass = prop.getProperty("db.pass", "postgres");
		
		// Config db
		String dbHost1 = prop.getProperty("db1.host", "127.0.0.1");
		String dbPort1 = prop.getProperty("db1.port", "5432");
		String dbName1 = prop.getProperty("db1.name", "helpdesk");
		String dbUser1 = prop.getProperty("db1.user", "postgres");
		String dbPass1 = prop.getProperty("db1.pass", "postgres");
		
		// Config dbSql
		String dbSqlHost = prop.getProperty("dbsql.host", "127.0.0.1");
		String dbSqlPort = prop.getProperty("dbsql.port", "5432");
		String dbSqlName = prop.getProperty("dbsql.name", "helpdesk");
		String dbSqlUser = prop.getProperty("dbsql.user", "postgres");
		String dbSqlPass = prop.getProperty("dbsql.pass", "postgres");
		
		Server server= new Server(port, minThread, maxThread, timeOutMilis, 
				dbHost, dbPort, dbName, dbUser, dbPass,
				dbSqlHost, dbSqlPort, dbSqlName, dbSqlUser, dbSqlPass,
				dbHost1, dbPort1, dbName1, dbUser1, dbPass1);
	}
	
	// Fungdi loadProperties
	public static void loadProperties(Properties prop, String filename) {
		InputStream input = null;
		try {
			input = new FileInputStream(filename);
			//Load properties file
			prop.load(input);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(input != null) {
				try {
					input.close();
				}
				catch (IOException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}
}
