package helpdesk.WebSocket;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import helpdesk.Utils.PingService;


@WebSocket
public class WSAssets {
	
	static final Set<Session> session = new CopyOnWriteArraySet<>();
	
	@OnWebSocketConnect
	public void onConnect(Session user) throws Exception{
		System.out.println("@OnWebSocketConnect: " + user.getRemoteAddress().getHostName() + ":" 
				+ user.getRemoteAddress().getPort());
		session.add(user);
	}
	
	@OnWebSocketClose
	public void onClose(Session user, int statusCode, String reason) {
		System.out.println("@OnWebSocketClose: " + user.getRemoteAddress().getHostString() + ":" + statusCode + ", " + reason);
		session.remove(user);
	}
	
	@OnWebSocketMessage
	public void onMessage(Session user, String message) {
		System.out.println("@OnWebSocketMessage: " + message);
		broadcastMessage(message);
		try {
			JsonObject jobj = new Gson().fromJson(message, JsonObject.class);
			
			String destination = jobj.get("ping").getAsString();
			PingService ps = new PingService(destination);
			privateMessage(ps.pingDestination(), user);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@OnWebSocketError
	public void onError (Session user, Throwable ex) {
		System.out.println("@OnWebSocketError: " + ex.getMessage());
	}
	
	public static void broadcastMessage(String msg) {
		session.stream().filter(Session::isOpen).forEach(session->{
			try {
				//session.getRemote().sendString(String.valueOf(new JSONObject().put("data", msg)));
				session.getRemote().sendString(msg);
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		});
	}

	public static void privateMessage(String msg, Session destination) {
		session.stream().filter(Session::isOpen).forEach(session->{
			// Send response to specific user
			if(session.getRemoteAddress().equals(destination.getRemoteAddress())) {
				try {
					session.getRemote().sendString(String.valueOf(new JSONObject().put("data", msg)));
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
	}

}
