package helpdesk.WebSocket;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArraySet;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import helpdesk.Utils.PingService;


@WebSocket
public class WebSocketHandler {
	
	static final Set<Session> session = new CopyOnWriteArraySet<>();
	Set<Session> tmpsession = new CopyOnWriteArraySet<>();
	int tglBefore = 0;
	
	public WebSocketHandler() {
		// TODO Auto-generated constructor stub
		
		Calendar calendar = Calendar.getInstance();
		int minuteNow = calendar.get(Calendar.MINUTE);
		
		long milisLeftToHour = (61 - minuteNow) * 60 * 1000;
		
//		Timer timer = new Timer();
		// cek setiap jam, apakah sudah ganti hari jika, ganti maka cek client
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println("Running: " + new java.util.Date());
//                int tglNow = calendar.get(Calendar.DAY_OF_MONTH);
//                
//                if(tglBefore != tglNow) {
//                	// Jika sudah pindah hari lakukan pengecekan client yg connect websocket
//                	sendPingPong();
//                }
//                tglBefore = tglNow;
//
//            }
//        }, milisLeftToHour, 3600000);
		// cek setiap menit, cek apakah client masih konek
//		timer.schedule(new TimerTask() {
//          @Override
//          public void run() {
//              System.out.println("Running: " + new java.util.Date());
//              	// lakukan pengecekan client yg connect websocket
//              	sendPingPong();
//          }
//		}, 60000, 60000);
	}
	
	@OnWebSocketConnect
	public void onConnect(Session user) throws Exception{
		System.out.println("@OnWebSocketConnect: " + user.getRemoteAddress().getHostName() + ":" 
				+ user.getRemoteAddress().getPort());
		session.add(user);
	}
	
	@OnWebSocketClose
	public void onClose(Session user, int statusCode, String reason) {
		System.out.println("@OnWebSocketClose: " + user.getRemoteAddress().getHostString() + ":" + statusCode + ", " + reason);
		session.remove(user);
	}
	
	@OnWebSocketMessage
	public void onMessage(Session user, String message) {
		System.out.println("@OnWebSocketMessage: " + message);
		broadcastMessage(message);
		try {
			JsonObject jobj = new Gson().fromJson(message, JsonObject.class);
			
			String kode = jobj.get("kode").getAsString();
			
			translateCode(kode, user);
			
//			PingService ps = new PingService("10.10.12.20");
//			privateMessage(ps.pingDestination(), user);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@OnWebSocketError
	public void onError (Session user, Throwable ex) {
		System.out.println("@OnWebSocketError: " + ex.getMessage());
	}
	
	public static void broadcastMessage(String msg) {
		session.stream().filter(Session::isOpen).forEach(session->{
			try {
				//session.getRemote().sendString(String.valueOf(new JSONObject().put("data", msg)));
				session.getRemote().sendString(msg);
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		});
	}

	public static void privateMessage(String msg, Session destination) {
		session.stream().filter(Session::isOpen).forEach(session->{
			// Send response to specific user
			if(session.getRemoteAddress().equals(destination.getRemoteAddress())) {
				try {
					session.getRemote().sendString(String.valueOf(new JSONObject().put("data", msg)));
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
	}
	
	private void sendPingPong() {
		
		JsonObject jObj = new JsonObject();
		jObj.addProperty("kode", "cek");
		jObj.addProperty("data", "");
		
//		String msg = jObj.toString();
		
		session.stream().filter(Session::isOpen).forEach(session->{
			try {
				session.getRemote().sendString(String.valueOf(jObj));
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		});
		
		Timer closetimer = new Timer();
		closetimer.schedule(new TimerTask() {
            @Override
            public void run() {
            	Set<Session> copy = new HashSet<Session>(session); 
            	copy.removeAll(tmpsession);
            	copy.stream().filter(Session::isOpen).forEach(sessionTmp->{
        			try {
        				sessionTmp.close();
        				session.remove(sessionTmp);
        			}
        			catch (Exception e) {
        				// TODO: handle exception
        				e.printStackTrace();
        			}
        		});
            	
            	JsonObject jObj2 = new JsonObject();
            	
            	JsonArray jArr = new JsonArray();
            	
            	session.stream().filter(Session::isOpen).forEach(session->{
        			try {
        				JsonObject joUser = new JsonObject();
        				joUser.addProperty("nama", session.getRemoteAddress().getHostName() + ":" 
        						+ session.getRemoteAddress().getPort());
        				jArr.add(joUser);
        			}
        			catch (Exception e) {
        				// TODO: handle exception
        				e.printStackTrace();
        			}
        		});
            	
            	
        		jObj2.addProperty("kode", "list_user");
        		jObj2.add("data", jArr);
        		
            	broadcastMessage(String.valueOf(jObj2));
            }
        }, 5000);
	}
	
	private void translateCode(String strCode, Session user) {
		switch (strCode) {
		case "cok":
			tmpsession.add(user);
			break;

		default:
			break;
		}
	}

}
