package helpdesk.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import spark.ResponseTransformer;

public class JSONTransformer implements ResponseTransformer{
	
	ObjectMapper objMapper = new ObjectMapper();

	@Override
	public String render(Object model) throws Exception {
		// TODO Auto-generated method stub
		objMapper.enable(SerializationFeature.INDENT_OUTPUT);
		return objMapper.writeValueAsString(model);
	}

}
