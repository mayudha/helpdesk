package helpdesk.Utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class PingService {
	
	String ipDestination;
	
	public PingService(String ipDestination) {
		this.ipDestination = ipDestination;
		
	}
	
	public String pingDestination() {
		InetAddress host;
		String result="";
		try {
			host = InetAddress.getByName(this.ipDestination);
			String strSuccess="";
			boolean isSuccess = host.isReachable(1000);
			System.out.println("host.isReachable(1000) = " + isSuccess);
			if(isSuccess)
				strSuccess = "Success";
			else
				strSuccess = "Fail";
			result= "Ping to " + this.ipDestination + ", with 1 second timeout, " + strSuccess;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result= "Ping to " + this.ipDestination + " error: " + e.getMessage();
		}catch(IOException ioE) {
			ioE.printStackTrace();
			result= "Ping to " + this.ipDestination + " error: " + ioE.getMessage();
		}
		
		return result;
	}
}
